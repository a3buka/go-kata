package main

import (
	"fmt"
	"sync"
	"time"
)

func joinChannels(chs ...<-chan int) <-chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func sleep() {
	time.Sleep(30 * time.Second)
}

func main() {

	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	go func() {
		for _, num := range []int{1, 2, 3} {
			a <- num
		}
		sleep()
		close(a)
	}()

	go func() {
		for _, num := range []int{20, 10, 30} {
			b <- num
		}
		sleep()
		close(b)
	}()

	go func() {
		for _, num := range []int{300, 200, 100} {
			c <- num
		}
		sleep()
		close(c)
	}()

	tick := make(chan bool)
	ticker := time.NewTicker(1 * time.Second)

	go func() {
		for {
			select {
			case <-tick:
				return
			case t := <-ticker.C:
				fmt.Println("time", t)
			}
		}

	}()

	time.Sleep(30 * time.Second)
	ticker.Stop()
	tick <- true

	for num := range joinChannels(a, b, c) {
		fmt.Println(num)
	}

}
