package main

import (
	"context"
	"fmt"
	"sync"
	"time"
)

//Объясниет пожалуйста за cancel() зачем он нужен, и почему линтер ругается если я его отбрасываю?

func joinChannels(chs ...<-chan int) <-chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func main() {
	head := context.Background()
	//ctx, _ := context.WithTimeout(head, 3*time.Second)
	//defer cancel()
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	go func() {
		newCtx, cancel := context.WithTimeout(head, 30*time.Second)
		defer cancel()

		for _, num := range []int{1, 2, 3} {
			a <- num
		}
		select {
		case <-newCtx.Done():
			close(a)
		default:
			return

		}
	}()

	go func() {
		newCtx, cancel := context.WithTimeout(head, 30*time.Second)
		defer cancel()
		for _, num := range []int{20, 10, 30} {
			b <- num
		}
		select {
		case <-newCtx.Done():
			close(b)
		default:
			return
		}
	}()

	go func() {
		newCtx, cancel := context.WithTimeout(head, 30*time.Second)
		defer cancel()

		for _, num := range []int{300, 200, 100} {
			c <- num
		}
		select {
		case <-newCtx.Done():
			close(c)
		default:
			return
		}

	}()

	ticker := time.NewTicker(1 * time.Second)
	tick := make(chan bool)

	go func() {
		for {
			select {
			case <-tick:
				return
			case t := <-ticker.C:
				fmt.Println("at time:", t)
			}
		}
	}()

	time.Sleep(30 * time.Second)
	ticker.Stop()

	tick <- true

	for num := range joinChannels(a, b, c) {
		fmt.Println(num)
	}
}
