package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewCalcSum(t *testing.T) {

	type testcase struct {
		name   string
		a      float64
		b      float64
		result float64
	}
	calc := NewCalc()
	testSum := []testcase{
		{
			name:   "Значение 2 и 2 сумма 4",
			a:      2,
			b:      2,
			result: 4,
		},
		{
			name:   "Значение 3 и 3 сумма 6",
			a:      3,
			b:      3,
			result: 6,
		},
		{
			name:   "Значение 3 и 3 сумма 6",
			a:      4,
			b:      4,
			result: 8,
		},
		{
			name:   "Значение 3 и 3 сумма 6",
			a:      5,
			b:      5,
			result: 10,
		},
		{
			name:   "Значение 3 и 3 сумма 6",
			a:      6,
			b:      6,
			result: 12,
		},
		{
			name:   "Значение 3 и 3 сумма 6",
			a:      3,
			b:      6,
			result: 9,
		},
		{
			name:   "Значение 3 и 3 сумма 6",
			a:      10,
			b:      1,
			result: 11,
		},
		{
			name:   "Значение 3 и 3 сумма 6",
			a:      20,
			b:      68,
			result: 88,
		},
	}

	for _, test := range testSum {

		t.Run(test.name, func(t *testing.T) {
			assert.Equalf(t, test.result, calc.SetA(test.a).SetB(test.b).Do(Sum).Result(), "Sum(%v, %v)", test.a, test.b)
		})
	}
}
