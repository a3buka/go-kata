package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewCalcDivide(t *testing.T) {

	type testcase struct {
		name   string
		a      float64
		b      float64
		result float64
	}
	calc := NewCalc()
	testSum := []testcase{
		{
			name:   "деление",
			a:      2,
			b:      2,
			result: 1,
		},
		{
			name:   "деление",
			a:      3,
			b:      3,
			result: 1,
		},
		{
			name:   "деление",
			a:      4,
			b:      4,
			result: 1,
		},
		{
			name:   "деление",
			a:      5,
			b:      5,
			result: 1,
		},
		{
			name:   "деление",
			a:      6,
			b:      6,
			result: 1,
		},
		{
			name:   "деление",
			a:      3,
			b:      6,
			result: 0.5,
		},
		{
			name:   "деление",
			a:      10,
			b:      1,
			result: 10,
		},
		{
			name:   "деление",
			a:      7,
			b:      6,
			result: 1.1666666666666667,
		},
	}

	for _, test := range testSum {

		t.Run(test.name, func(t *testing.T) {
			assert.Equalf(t, test.result, calc.SetA(test.a).SetB(test.b).Do(divide).Result(), "Sum(%v, %v)", test.a, test.b)
		})
	}
}
