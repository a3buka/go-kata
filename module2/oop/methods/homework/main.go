package main

import "fmt"

type calc struct {
	a, b   float64
	result float64
}

func NewCalc() *calc { // конструктор калькулятора
	return &calc{}
}

func (c calc) SetA(a float64) *calc {
	c.a = a
	return &c
}

func (c calc) SetB(b float64) *calc {
	c.b = b
	return &c
}

func (c calc) Do(operation func(a, b float64) float64) *calc {
	c.result = operation(c.a, c.b)
	return &c
}

func (c calc) Result() float64 {
	return c.result
}

func multiply(a, b float64) float64 { // реализуйте по примеру divide, sum, average
	return a * b
}

func Sum(a, b float64) float64 {
	return a + b
}

func divide(a, b float64) float64 {
	if b != 0 {
		return a / b
	} else if b == 0 {
		panic("На 0 делить нельзя!")
	}
	return b
}

func average(a, b float64) float64 {
	return a - b
}

func main() {
	calc := NewCalc()
	res := calc.SetA(30).SetB(10).Do(multiply).Result()
	fmt.Println("Multiply:", res)
	res2 := calc.SetA(30).SetB(10).Do(Sum).Result()
	fmt.Println("Sum:", res2)
	res3 := calc.SetA(30).SetB(10).Do(average).Result()
	fmt.Println("Average", res3)
	res4 := calc.SetA(30).SetB(10).Do(divide).Result()
	fmt.Println("Divide", res4)
}
