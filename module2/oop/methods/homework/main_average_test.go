package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewCalcAverage(t *testing.T) {

	type testcase struct {
		name   string
		a      float64
		b      float64
		result float64
	}
	calc := NewCalc()
	testSum := []testcase{
		{
			name:   "вычитание",
			a:      2,
			b:      2,
			result: 0,
		},
		{
			name:   "вычитание",
			a:      3,
			b:      3,
			result: 0,
		},
		{
			name:   "вычитание",
			a:      4,
			b:      4,
			result: 0,
		},
		{
			name:   "вычитание",
			a:      5,
			b:      5,
			result: 0,
		},
		{
			name:   "вычитание",
			a:      6,
			b:      6,
			result: 0,
		},
		{
			name:   "вычитание",
			a:      3,
			b:      6,
			result: -3,
		},
		{
			name:   "вычитание",
			a:      10,
			b:      1,
			result: 9,
		},
		{
			name:   "вычитание",
			a:      7,
			b:      6,
			result: 1,
		},
	}

	for _, test := range testSum {

		t.Run(test.name, func(t *testing.T) {
			assert.Equalf(t, test.result, calc.SetA(test.a).SetB(test.b).Do(average).Result(), "Sum(%v, %v)", test.a, test.b)
		})
	}
}
