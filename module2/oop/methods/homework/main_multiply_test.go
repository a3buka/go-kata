package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewCalcMultiply(t *testing.T) {

	type testcase struct {
		name   string
		a      float64
		b      float64
		result float64
	}
	calc := NewCalc()
	testSum := []testcase{
		{
			name:   "умножение",
			a:      2,
			b:      2,
			result: 4,
		},
		{
			name:   "умножение",
			a:      3,
			b:      3,
			result: 9,
		},
		{
			name:   "умножение",
			a:      4,
			b:      4,
			result: 16,
		},
		{
			name:   "умножение",
			a:      5,
			b:      5,
			result: 25,
		},
		{
			name:   "умножение",
			a:      6,
			b:      6,
			result: 36,
		},
		{
			name:   "умножение",
			a:      3,
			b:      6,
			result: 18,
		},
		{
			name:   "умножение",
			a:      10,
			b:      1,
			result: 10,
		},
		{
			name:   "умножение",
			a:      2,
			b:      6,
			result: 12,
		},
	}

	for _, test := range testSum {

		t.Run(test.name, func(t *testing.T) {
			assert.Equalf(t, test.result, calc.SetA(test.a).SetB(test.b).Do(multiply).Result(), "Sum(%v, %v)", test.a, test.b)
		})
	}
}
