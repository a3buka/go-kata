package main

import (
	"fmt"

	"gitlab.com/a3buka/greet"
)

func main() {
	greet.Hello()

	fmt.Println(greet.Shark)

	oct := greet.Octopus{
		Name:  "Jessy",
		Color: "orange",
	}
	fmt.Println(oct.String())

	oct.Reset()

	fmt.Println(oct.String())
}
