module gitlab.com/a3buka/go-kata/module2/packages/gomapper/homework

go 1.19

require (
	github.com/json-iterator/go v1.1.12
	github.com/ptflp/godecoder v0.0.1
)

require (
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
)
