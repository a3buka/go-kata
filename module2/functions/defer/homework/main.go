package main

import (
	"errors"
	"fmt"
	"sort"
)

// MergeDictsJob is a job to merge dictionaries into a single dictionary.
type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

// errors
var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

// BEGIN (write your solution here)

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	keys := make([]string, 0, 5)

	if len(job.Dicts) < 2 {
		return job, errNotEnoughDicts
	}

	for i := 0; i < len(job.Dicts); i++ {
		if job.Dicts[i] == nil {
			return job, errNilDict
		}
	}

	for i := 0; i < len(job.Dicts); i++ {
		for j := range job.Dicts[i] {
			keys = append(keys, j)
		}
	}
	sort.Strings(keys)
	dictionary := map[string]string{}
	for _, f := range keys {
		for i := 0; i < len(job.Dicts); i++ {
			for j := range job.Dicts[i] {
				if f == j {
					dictionary[f] = job.Dicts[i][j]

				}
			}
		}
	}

	job.Merged = dictionary
	return job, nil
}

func main() {
	dictionary := []map[string]string{{"a": "b"}, {"g": "k"}, nil}
	job := &MergeDictsJob{
		Dicts:      dictionary,
		Merged:     nil,
		IsFinished: true,
	}
	_, err := ExecuteMergeDictsJob(job)
	fmt.Println(job.IsFinished, job.Merged, err)

}

// END

// Пример работы
// ExecuteMergeDictsJob(&MergeDictsJob{}) // &MergeDictsJob{IsFinished: true}, "at least 2 dictionaries are required"
// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},nil}}) // &MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b"},nil}}, "nil dictionary"
// ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"},{"b": "c"}}}) // &MergeDictsJob{IsFinished: true, Dicts: []map[string]string{{"a": "b", "b": "c"}}}, nil
