package main

import "testing"

// Не могу понять разные виды объявлений бенчмарков дают разные результаты
// Почему скорость обработки разная я понимаю, но как тогда во втором варианте такая большая разница появляется
// Вариант 1
func BenchmarkMapUserProducts(b *testing.B) {
	b.ResetTimer()
	for j := 0; j < b.N; j++ {
		MapUserProducts(GenUsers(), GenProducts())
	}
	b.StopTimer()
}

func BenchmarkMapUserProducts2(b *testing.B) {

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		MapUserProducts2(GenUsers(), GenProducts())
	}
	b.StopTimer()
}

// Вариант 2
func BenchmarkMapUserProducts_var2(b *testing.B) {
	users := GenUsers()
	products := GenProducts()
	b.ResetTimer()
	for j := 0; j < b.N; j++ {
		MapUserProducts(users, products)
	}
	b.StopTimer()
}

func BenchmarkMapUserProducts2_var2(b *testing.B) {
	users := GenUsers()
	products := GenProducts()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		MapUserProducts2(users, products)
	}
	b.StopTimer()
}
