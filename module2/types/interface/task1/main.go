package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n *int
	//fmt.Println(n == nil)
	test(n)
}

func test(r interface{}) {
	switch r.(type) {
	case int:
		fmt.Println("int")
	case string:
		fmt.Println("string")
	case nil:
		fmt.Println("nil")
	default:
		fmt.Println("Success!")
	}

}
