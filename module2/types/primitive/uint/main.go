package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("size is:", unsafe.Sizeof(n), "bytes")

	typeUint()
}

func typeUint() {
	fmt.Println("----START TYPE UINT----")
	var numberUint8 uint8 = 1 << 1
	fmt.Println("left shift uint8:", numberUint8, "size", unsafe.Sizeof(numberUint8), "bytes")
	var numberUint8r uint8 = 1 >> 1
	fmt.Println("right shift uint8:", numberUint8r, "size", unsafe.Sizeof(numberUint8r), "bytes")
	var numberUint8Max uint8 = 1<<8 - 1
	fmt.Println("uint8 max value:", numberUint8Max, "size", unsafe.Sizeof(numberUint8Max), "bytes")
	var numberUint16Max uint16 = 1<<16 - 1
	fmt.Println("uint16 max value:", numberUint16Max, "size", unsafe.Sizeof(numberUint16Max), "bytes")
	var numberUint32Max uint32 = 1<<32 - 1
	fmt.Println("uint32 max value:", numberUint32Max, "size", unsafe.Sizeof(numberUint32Max), "bytes")
	var numberUint64Max uint64 = 1<<64 - 1
	fmt.Println("uint64 max value:", numberUint64Max, "size", unsafe.Sizeof(numberUint64Max), "bytes")
	fmt.Println("----END TYPE UINT----")

}
