package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeInt()
}

func typeInt() {
	fmt.Println("----START TYPE INT----")
	var uintNumber8 uint8 = 1 << 7
	var numberInt8Max = int8(uintNumber8 - 1)
	var numberInt8Min = int8(uintNumber8)
	fmt.Println("Max value int8:", numberInt8Max, "Min value int8:", numberInt8Min, "size", unsafe.Sizeof(numberInt8Min), "bytes")
	var uintNumber16 uint16 = 1 << 15
	var numberInt16Max = int16(uintNumber16 - 1)
	var numberInt16Min = int16(uintNumber16)
	fmt.Println("Max value int16:", numberInt16Max, "Min value int16:", numberInt16Min, "size", unsafe.Sizeof(numberInt16Min), "bytes")
	var uintNumber32 uint32 = 1 << 31
	var numberInt32Max = int32(uintNumber32 - 1)
	var numberInt32Min = int32(uintNumber32)
	fmt.Println("Max value int32:", numberInt32Max, "Min value int32:", numberInt32Min, "size", unsafe.Sizeof(numberInt32Min), "bytes")
	var uintNumber64 uint64 = 1 << 63
	var numberInt64Max = int64(uintNumber64 - 1)
	var numberInt64Min = int64(uintNumber64)
	fmt.Println("Max value int64:", numberInt64Max, "Min value int64:", numberInt64Min, "size", unsafe.Sizeof(numberInt8Min), "bytes")
	fmt.Println("----END TYPE INT----")

}
