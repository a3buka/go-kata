package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/LAION-AI/Open-Assistant",
			Stars: 1576,
		},
		{
			Name:  "https://github.com/apple/ml-stable-diffusion",
			Stars: 4625,
		},
		{
			Name:  "https://github.com/hehonghui/awesome-english-ebooks",
			Stars: 3222,
		},
		{
			Name:  "https://github.com/AmruthPillai/Reactive-Resume",
			Stars: 8986,
		},
		{
			Name:  "https://github.com/microfeed/microfeed",
			Stars: 1187,
		},
		{
			Name:  "https://github.com/microfeed/microfeed",
			Stars: 38077,
		},
		{
			Name:  "https://github.com/TheOfficialFloW/HENlo",
			Stars: 154,
		},
		{
			Name:  "https://github.com/tesseract-ocr/tesseract",
			Stars: 8301,
		},
		{
			Name:  "https://github.com/tesseract-ocr/tesseract,",
			Stars: 15906,
		},
		{
			Name:  "https://github.com/timqian/chinese-independent-blogs",
			Stars: 11715,
		},
		{
			Name:  "https://github.com/timqian/chinese-independent-blogs",
			Stars: 1406,
		},
		{
			Name:  "https://github.com/lencx/ChatGPT",
			Stars: 2509,
		},
	}
	link := make(map[string]Project)
	for i, ok := range projects {
		link[projects[i].Name] = ok
	}
	for ok := range link {
		fmt.Println(link[ok])
	}
}
