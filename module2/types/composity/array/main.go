package main

import "fmt"

type User struct {
	name   string
	age    int
	Wallet Wallet
}

type Wallet struct {
	RUB uint64
	USD uint64
	BTC uint64
	ETH uint64
}

func main() {
	testArray()
	rangeArray()
}

func testArray() {
	a := [...]int{34, 55, 89, 144}
	fmt.Println("original value", a)
	a[0] = 21
	fmt.Println("channged first element", a)
	b := a
	a[0] = 233
	fmt.Println("original value", a)
	fmt.Println("modified value", b)
}

func rangeArray() {
	users := [4]User{
		{
			age:  8,
			name: "John",
			Wallet: Wallet{
				RUB: 13,
				USD: 1,
				BTC: 0,
				ETH: 0,
			},
		},
		{
			age:  13,
			name: "Katie",
			Wallet: Wallet{
				RUB: 500,
				USD: 3,
				BTC: 0,
				ETH: 0,
			},
		},
		{
			age:  21,
			name: "Doe",
			Wallet: Wallet{
				RUB: 0,
				USD: 300,
				BTC: 1,
				ETH: 3,
			},
		},
		{
			age:  34,
			name: "Arine",
			Wallet: Wallet{
				RUB: 932420,
				USD: 34,
				BTC: 1,
				ETH: 3,
			},
		},
	}
	fmt.Println("У пользователя есть крипта")
	for i := range users {
		if users[i].Wallet.BTC > 0 || users[i].Wallet.ETH > 0 {
			fmt.Println(users[i])
		}
	}
}
