package main

import "fmt"

//Я честно говоря не понял что вы мне написали в комментарии по исправлению
//поэтому добавил еще два возможных варианта

func main() {
	s := []int{1, 2, 3}
	d := []int{1, 2, 3}
	f := []int{1, 2, 3}
	s1 := []int{1, 2, 3}

	Append(&s)
	d = Append2(d, 4)
	f = append(f, 4)

	fmt.Println(s)
	fmt.Println(d)
	fmt.Println(f)
	fmt.Println(Append1(s1))
}

func Append(s *[]int) {
	*s = append(*s, 4)
}

func Append1(s1 []int) []int {
	s1 = append(s1, 4)
	return s1
}

func Append2(d []int, n int) []int {
	if len(d) == cap(d) {
		newD := make([]int, len(d), 2*cap(d)+1)
		copy(newD, d)
		d = newD
	}
	d = d[:4]
	d[3] = n
	return d
}
