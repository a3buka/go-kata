package main

import (
	"fmt"
	"unsafe"
)

type User struct {
	name     string
	age      int
	Wallet   Wallet
	Location Location
}

type Location struct {
	address string
	city    string
	index   string
}

type Wallet struct {
	RUB uint64
	USD uint64
	BTC uint64
	ETH uint64
}

func main() {
	user := User{
		age:  13,
		name: "Alexandr",
	}
	fmt.Println(user)

	wallet := Wallet{
		RUB: 35000,
		USD: 2500,
		BTC: 1,
		ETH: 1,
	}
	fmt.Println(wallet)
	fmt.Println("wallet allocates", unsafe.Sizeof(wallet), "bytes")
	user.Wallet = wallet
	fmt.Println(user)

	user2 := User{
		age:  14,
		name: "Petr",
		Wallet: Wallet{
			RUB: 17000,
			USD: 2234,
			BTC: 1,
			ETH: 2,
		},
		Location: Location{
			city:    "Moscow",
			address: "Новокузнецчкая 34",
			index:   "443000",
		},
	}
	fmt.Println(user2)

}
