package main

import (
	"fmt"
	"unsafe"
)

func main() {
	typeBool()
}

func typeBool() {
	var b bool
	fmt.Println("Размер в байтах:", unsafe.Sizeof(b))
	var v uint8 = 1
	fmt.Println(b)
	b = *(*bool)(unsafe.Pointer(&v))
	fmt.Println(b)
}
