package main

import (
	"encoding/json"
	"fmt"
	"os"

	jsoniter "github.com/json-iterator/go"
)

type Kata []KataElement

func UnmarshalKata(data []byte) (Kata, error) {
	var r Kata
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Kata) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

func UnmarshalKata1(data []byte) (Kata, error) {
	var r Kata
	err := jsoniter.Unmarshal(data, &r)
	return r, err
}

func (r *Kata) Marshal1() ([]byte, error) {
	return jsoniter.Marshal(r)
}

type KataElement struct {
	ID        float64    `json:"id"`
	Category  Category   `json:"category"`
	Name      string     `json:"name"`
	PhotoUrls []string   `json:"photoUrls"`
	Tags      []Category `json:"tags"`
	Status    string     `json:"status"`
}

type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

func main() {
	text, _ := os.ReadFile("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module2/optimizate/json/homework/jsonData.json")
	jsonData, _ := UnmarshalKata(text)
	kata, _ := jsonData.Marshal()
	_ = kata
	fmt.Println(jsonData)

	jsonData1, _ := UnmarshalKata1(text)
	kata1, _ := jsonData1.Marshal1()
	_ = kata1
	fmt.Println(jsonData1)

}
