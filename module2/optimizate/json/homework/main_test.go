package main

import (
	"os"
	"testing"
)

func BenchmarkStandardJson(b *testing.B) {
	var (
		kata Kata
		err  error
		data []byte
	)
	jsonData, _ := os.ReadFile("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module2/optimizate/json/homework/jsonData.json")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		kata, err = UnmarshalKata(jsonData)
		if err != nil {
			panic(err)
		}
		data, err = kata.Marshal()
		if err != nil {
			panic(err)
		}
		_ = data
	}
	b.StopTimer()
}

func BenchmarkJsonIter(b *testing.B) {
	var (
		kata Kata
		err  error
		data []byte
	)
	jsonData, _ := os.ReadFile("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module2/optimizate/json/homework/jsonData.json")
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		kata, err = UnmarshalKata1(jsonData)
		if err != nil {
			panic(err)
		}
		data, err = kata.Marshal1()
		if err != nil {
			panic(err)
		}
		_ = data
	}
	b.StopTimer()
}
