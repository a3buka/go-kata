package main

import "testing"

func BenchmarkFilterText_SanitizeTextNew(b *testing.B) {
	ft := NewFilterText()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for j := range data {
			ft.SanitizeText(data[j])
		}
	}
}

func BenchmarkFilterText_SanitizeTextOld(b *testing.B) {

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for j := range data {
			SanitizeText1(data[j])
		}
	}
}
