package main

import (
	"fmt"
)

type Person struct {
	Name  string
	Age   int
	Money float64
}

func generateSelfStory(name string, age int, money float64) {
	fmt.Printf("Hello! My name is %s. I'am %v y.o. And i also have $%-6.2fin my wallet right now.\n", name, age, money)
}

func main() {
	p := Person{Name: "Andy", Age: 18, Money: 0}
	tom := Person{Name: "Tom", Age: 25, Money: 10.0000032}
	generateSelfStory(tom.Name, tom.Age, tom.Money)
	// вывод значений структуры
	fmt.Println("simple struct:", p)

	// вывод названий полей и их значений
	fmt.Printf("detailed struct: %+v\n", p)

	// вывод названий полей и их значений в виде инициализации
	fmt.Printf("Golang struct: %#v\n", p)
}
