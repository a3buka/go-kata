module gitlab.com/a3buka/go-kata/module2/stl/ut8/homework

go 1.19

require (
	github.com/mxmCherry/translit v1.0.0
	golang.org/x/text v0.7.0
)

require (
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.26.0 // indirect
)
