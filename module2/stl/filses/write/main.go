package main

import (
	"os"
	"strings"
)

var dic = map[string]string{"ь": "", "ъ": "", "а": "a", "б": "b", "в": "v",
	"г": "g", "д": "d", "е": "e", "ё": "yo", "ж": "zh",
	"з": "z", "и": "i", "й": "y", "к": "k", "л": "l",
	"м": "m", "н": "n", "о": "o", "п": "p", "р": "r",
	"с": "s", "т": "t", "у": "u", "ф": "f", "х": "h",
	"ц": "ts", "ч": "ch", "ш": "sh", "щ": "sch",
	"ы": "yi", "э": "e", "ю": "yu", "я": "ya"}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {

	//reader := bufio.NewReader(os.Stdin)
	//fmt.Print("Enter your name: ")
	//name, _ := reader.ReadString('\n')
	//fmt.Printf("Hello %s\n", name)

	text, err := os.ReadFile("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module2/stl/filses/write/processed.txt.txt")
	check(err)
	fileText := string(text[:])
	fileSplit := strings.Split(fileText, " ")
	text_eng, err := os.OpenFile("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module2/stl/filses/write/example.processed.txt",
		os.O_WRONLY|os.O_RDONLY|os.O_CREATE, os.FileMode(0744))
	check(err)
	for i := range fileSplit {
		_, err = text_eng.Write([]byte(dic[fileSplit[i]]))
		check(err)
	}

}
