package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
)

type Config struct {
	AppName    string `json:"app"`
	Production bool   `json:"Product"`
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {

	file, _ := os.ReadFile("./configs.json")
	conf := Config{}
	err := json.Unmarshal(file, &conf)
	check(err)

	config := flag.Bool("conf", true, "")
	flag.Parse()

	if *config {
		fmt.Printf("AppName: %v\nProduction: %v", conf.AppName, conf.Production)

	}

	//fmt.Printf("AppName: %v\nProduction: %v", conf.AppName, conf.Production)

}

// Пример вывода конфига
// Production: true
// AppName: мое тестовое приложение
