package main

import (
	"bytes"
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	file, err := os.OpenFile("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module2/stl/io/homework/example.txt.",
		os.O_CREATE|os.O_APPEND|os.O_RDONLY|os.O_WRONLY, 0700) //  создайте файл
	//defer file.Close()
	var text bytes.Buffer
	var new_data bytes.Buffer
	check(err)
	for _, i := range data {
		text.WriteString(i + "\n")
		_, err = file.WriteString(text.String()) //Записб данных в буфер
		check(err)
		text.Reset()

	}
	text2, err := os.ReadFile("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module2/stl/io/homework/example.txt.")
	check(err)
	new_data.WriteString(string(text2)) //прочтите данные в новом буфере

	fmt.Println(new_data.String()) // выведите данные из буфера buffer.String()
	// у вас все получится!
}
