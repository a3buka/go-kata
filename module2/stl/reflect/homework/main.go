package main

import (
	"fmt"
	"reflect"
	"time"
)

var reflectedStructs map[string][]Field

type Field struct {
	Name string
	Tags map[string]string
}

type UserDTO struct {
	ID            int       `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Name          string    `json:"name" db:"name" db_type:"varchar(55)" db_default:"default null" db_ops:"create,update"`
	Phone         string    `json:"phone" db:"phone" db_type:"varchar(34)" db_default:"default null" db_index:"index,unique" db_ops:"create,update"`
	Email         string    `json:"email" db:"email" db_type:"varchar(89)" db_default:"default null" db_index:"index,unique" db_ops:"create,update"`
	Password      string    `json:"password" db:"password" db_type:"varchar(144)" db_default:"default null" db_ops:"create,update"`
	Status        int       `json:"status" db:"status" db_type:"int" db_default:"default 0" db_ops:"create,update"`
	Role          int       `json:"role" db:"role" db_type:"int" db_default:"not null" db_ops:"create,update"`
	Verified      bool      `json:"verified" db:"verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	EmailVerified bool      `json:"email_verified" db:"email_verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	PhoneVerified bool      `json:"phone_verified" db:"phone_verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"` //
	CreatedAt     time.Time `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	UpdatedAt     time.Time `json:"updated_at" db:"updated_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	DeletedAt     time.Time `json:"deleted_at" db:"deleted_at" db_type:"timestamp" db_default:"default null" db_index:"index"`
}

type EmailVerifyDTO struct {
	ID        int       `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Email     string    `json:"email" db:"email" db_type:"varchar(89)" db_default:"not null" db_index:"index,unique" db_ops:"create,update"`
	UserID    int       `json:"user_id,omitempty" db:"user_id" db_ops:"create" db_type:"int" db_default:"not null" db_index:"index"`
	Hash      string    `json:"hash,omitempty" db:"hash" db_ops:"create" db_type:"char(36)" db_default:"not null" db_index:"index"`
	Verified  bool      `json:"verified" db:"verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	CreatedAt time.Time `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
}

func main() {
	structs := []interface{}{
		UserDTO{},
		EmailVerifyDTO{},
	}

	//заполни данными структур согласно заданиям
	reflectedStructs = make(map[string][]Field, len(structs))

	// Вот тут пытался сделать через циклы но сдался у меня не получается!!!!

	for i := range structs {
		elemInterface := reflect.TypeOf(structs[i])
		Data := make([]Field, elemInterface.NumField())
		for j := 0; j < elemInterface.NumField(); j++ { //elemInterface.NumField() reflectedStructs[elemInterface.Name()]

			Data[j] = Field{Name: elemInterface.Field(j).Name, Tags: map[string]string{"json": elemInterface.Field(j).Tag.Get("json"),
				"db": elemInterface.Field(j).Tag.Get("db"), "db_type": elemInterface.Field(j).Tag.Get("db_type"),
				"db_default": elemInterface.Field(j).Tag.Get("db_default")}}

			if elemInterface.Field(j).Tag.Get("db_ops") != "" {
				Data[j].Tags["db_ops"] = elemInterface.Field(j).Tag.Get("db_ops")

			}

			if elemInterface.Field(j).Tag.Get("db_index") != "" {
				Data[j].Tags["db_index"] = elemInterface.Field(j).Tag.Get("db_index")

			}

		}
		reflectedStructs[elemInterface.Name()] = Data
		fmt.Println(reflectedStructs)
	}
}

//elemInterface1 := reflect.TypeOf(structs[0])
////elemInterface2 := reflect.TypeOf(structs[1])
//
//reflectedStructs[elemInterface1.Name()] = []Field{{Name: elemInterface1.Field(0).Name, Tags: map[string]string{"json": elemInterface1.Field(0).Tag.Get("json"),
//	"db": elemInterface1.Field(0).Tag.Get("db"), "db_type": elemInterface1.Field(0).Tag.Get("db_type"), "db_default": elemInterface1.Field(0).Tag.Get("db_default")}},
//
//	{Name: elemInterface1.Field(1).Name, Tags: map[string]string{"json": elemInterface1.Field(1).Tag.Get("json"),
//		"db": elemInterface1.Field(1).Tag.Get("db"), "db_type": elemInterface1.Field(1).Tag.Get("db_type"), "db_default": elemInterface1.Field(1).Tag.Get("db_default"),
//	"dp_ops":elemInterface1.Field(1).Tag.Get("dp_ops")}},
//
//	{Name: elemInterface1.Field(2).Name, Tags: map[string]string{"json": elemInterface1.Field(2).Tag.Get("json"),
//		"db": elemInterface1.Field(2).Tag.Get("db"), "db_type": elemInterface1.Field(2).Tag.Get("db_type"),
//		"db_default": elemInterface1.Field(2).Tag.Get("db_default"),"db_index": elemInterface1.Field(2).Tag.Get("db_index"),
//		"dp_ops":elemInterface1.Field(2).Tag.Get("dp_ops")}},
//
//	{Name: elemInterface1.Field(3).Name, Tags: map[string]string{"json": elemInterface1.Field(3).Tag.Get("json"),
//		"db": elemInterface1.Field(3).Tag.Get("db"), "db_type": elemInterface1.Field(3).Tag.Get("db_type"),
//		"db_default": elemInterface1.Field(3).Tag.Get("db_default"),"db_index": elemInterface1.Field(3).Tag.Get("db_index"),
//		"dp_ops":elemInterface1.Field(3).Tag.Get("dp_ops")}},
//
//	{Name: elemInterface1.Field(5).Name, Tags: map[string]string{"json": elemInterface1.Field(5).Tag.Get("json"),
//		"db": elemInterface1.Field(5).Tag.Get("db"), "db_type": elemInterface1.Field(5).Tag.Get("db_type"), "db_default": elemInterface1.Field(5).Tag.Get("db_default"),
//		"dp_ops":elemInterface1.Field(5).Tag.Get("dp_ops")}},
//
//	{Name: elemInterface1.Field(6).Name, Tags: map[string]string{"json": elemInterface1.Field(6).Tag.Get("json"),
//		"db": elemInterface1.Field(6).Tag.Get("db"), "db_type": elemInterface1.Field(6).Tag.Get("db_type"), "db_default": elemInterface1.Field(6).Tag.Get("db_default"),
//		"dp_ops":elemInterface1.Field(6).Tag.Get("dp_ops")}},
//
//	{Name: elemInterface1.Field(7).Name, Tags: map[string]string{"json": elemInterface1.Field(7).Tag.Get("json"),
//		"db": elemInterface1.Field(7).Tag.Get("db"), "db_type": elemInterface1.Field(7).Tag.Get("db_type"), "db_default": elemInterface1.Field(7).Tag.Get("db_default"),
//		"dp_ops":elemInterface1.Field(7).Tag.Get("dp_ops")}},
//
//	{Name: elemInterface1.Field(8).Name, Tags: map[string]string{"json": elemInterface1.Field(8).Tag.Get("json"),
//		"db": elemInterface1.Field(8).Tag.Get("db"), "db_type": elemInterface1.Field(8).Tag.Get("db_type"), "db_default": elemInterface1.Field(8).Tag.Get("db_default"),
//		"dp_ops":elemInterface1.Field(8).Tag.Get("dp_ops")}},
//
//	{Name: elemInterface1.Field(9).Name, Tags: map[string]string{"json": elemInterface1.Field(9).Tag.Get("json"),
//		"db": elemInterface1.Field(9).Tag.Get("db"), "db_type": elemInterface1.Field(9).Tag.Get("db_type"), "db_default": elemInterface1.Field(9).Tag.Get("db_default"),
//		"dp_ops":elemInterface1.Field(9).Tag.Get("dp_ops")}},
//

//}
