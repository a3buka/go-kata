package main

import (
	"fmt"
	"math/rand"
	"os"
)

const (
	noError = iota
	generalError
)

func runtime(text string) {
	fmt.Println(text)
}
func main() {
	Feature1()
	Feature2()
	Feature3()
	Feature4()
	runtime("Hello world")
	roll := rand.Intn(100)
	if roll > 50 {
		os.Exit(noError)
	} else {
		os.Exit(generalError)
	}

}
