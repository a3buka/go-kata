package app

import (
	"context"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	_ "github.com/lib/pq"
	"gitlab.com/a3buka/go-kata/module4/webserver/http/homework/pkg/service"

	"gitlab.com/a3buka/go-kata/module4/webserver/http/homework/pkg/repository"

	"github.com/sirupsen/logrus"
	"gitlab.com/a3buka/go-kata/module4/webserver/http/homework/iternal/server"
	"gitlab.com/a3buka/go-kata/module4/webserver/http/homework/pkg/handler"
)

func Run() {
	logrus.SetFormatter(new(logrus.JSONFormatter))

	db, err := repository.NewPostgresDB()
	if err != nil {
		logrus.Fatalf("failed to initialization db %s", err.Error())
	}

	repos := repository.NewRepository(db)
	services := service.NewService(repos)
	handlers := handler.NewHandler(services)

	srv := server.NewServer(handlers.InitRoutes())
	go func() {
		if err := srv.Run(); !errors.Is(err, http.ErrServerClosed) {
			logrus.Errorf("server it's not worked %s", err.Error())
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	logrus.Print("TodoApp Shutting Down")

	if err := srv.Shutdown(context.Background()); err != nil {
		logrus.Errorf("error occured on server shutting down: %s", err.Error())
	}

	if err := db.Close(); err != nil {
		logrus.Errorf("error occured on db connection close: %s", err.Error())
	}

}
