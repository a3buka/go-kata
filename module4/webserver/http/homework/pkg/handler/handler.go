package handler

import (
	"fmt"
	"net/http"

	"gitlab.com/a3buka/go-kata/module4/webserver/http/homework/pkg/service"

	"github.com/go-chi/chi/v5"
)

type Handler struct {
	service *service.Service
}

func NewHandler(service *service.Service) *Handler {
	return &Handler{service: service}
}

func (h *Handler) InitRoutes() *chi.Mux {
	router := chi.NewRouter()
	router.Route("/", func(r chi.Router) {
		router.Get("/", Hello)
		router.Get("/users", h.GetUsers)
		router.Get("/users/{id}", h.GetUserID)
		router.Post("/upload/{FileName}", h.UploadFile)
		router.Get("/public/{FileName}", h.GetFile)
	})

	return router
}

func Hello(w http.ResponseWriter, request *http.Request) {
	fmt.Fprint(w, "Hello kata")
}
