package handler

import (
	"net/http"

	"github.com/sirupsen/logrus"
)

func newErrorResponse(w http.ResponseWriter, message string, status int) {
	logrus.Error(message)
	http.Error(w, message, status)
}
