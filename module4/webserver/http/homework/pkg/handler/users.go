package handler

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
)

func (h *Handler) GetUsers(w http.ResponseWriter, r *http.Request) {

	//Вызываем запрос в БД
	usersDB, err := h.service.GetAll()
	if err != nil {
		newErrorResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}

	jsonDB, err := json.Marshal(usersDB)
	if err != nil {
		newErrorResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, string(jsonDB))
}

func (h *Handler) GetUserID(w http.ResponseWriter, r *http.Request) {
	requiredRequest := chi.URLParam(r, "id")
	requiredRequestInt, err := strconv.Atoi(requiredRequest)
	if err != nil {
		newErrorResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}
	userDB, err := h.service.GetUserID(requiredRequestInt)
	if err != nil {
		newErrorResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}

	jsonDB, err := json.Marshal(userDB)
	if err != nil {
		newErrorResponse(w, err.Error(), http.StatusInternalServerError)
		return
	}
	fmt.Fprint(w, string(jsonDB))
}
