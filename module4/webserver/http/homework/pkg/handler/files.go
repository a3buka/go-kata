package handler

import (
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
)

func (h *Handler) GetFile(w http.ResponseWriter, r *http.Request) {
	fileName := chi.URLParam(r, "FileName")
	file, err := h.service.FindFile(fileName)
	if err != nil {
		newErrorResponse(w, err.Error(), http.StatusInternalServerError)
	} else {
		fmt.Fprint(w, file)
	}

}

func (h *Handler) UploadFile(w http.ResponseWriter, r *http.Request) {
	fileName := chi.URLParam(r, "FileName")
	err := h.service.CreateFile(fileName)
	if err != nil {
		newErrorResponse(w, err.Error(), http.StatusInternalServerError)
	} else {
		fmt.Fprint(w, "файл успешно добавлен")
	}

}
