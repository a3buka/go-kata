package repository

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
)

const postgresCreateTableUser = `CREATE TABLE IF NOT EXISTS users (
    id			serial,
    firstName	varchar,
    lastName 	varchar
    )
`
const postgresCreateTableFile = `CREATE TABLE IF NOT EXISTS file (
    file	varchar
    )
`

func NewPostgresDB() (*sql.DB, error) {
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	db, err := sql.Open("postgres", fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s",
		os.Getenv("DB_HOST"), os.Getenv("DB_PORT"),
		os.Getenv("DB_USER"), os.Getenv("DB_NAME"),
		os.Getenv("DB_PASSWORD"), "disable"))

	if err != nil {
		log.Fatal("postgres doesn't open", err)
	}
	err = db.Ping()
	if err != nil {
		return nil, err
	}

	_, err = db.Exec(postgresCreateTableUser)
	if err != nil {
		log.Fatalln(err)
	}

	_, err = db.Exec(postgresCreateTableFile)
	if err != nil {
		log.Fatalln(err)
	}

	return db, nil
}
