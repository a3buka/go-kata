package repository

import (
	"database/sql"

	"gitlab.com/a3buka/go-kata/module4/webserver/http/homework/iternal/entity"
)

type Users interface {
	GetAll() ([]entity.User, error)
	GetUserID(id int) (entity.User, error)
}

type Fails interface {
	CreateFile(fileName string) error
	FindFile(fileName string) (entity.File, error)
}
type Repository struct {
	Users
	Fails
}

func NewRepository(db *sql.DB) *Repository {
	return &Repository{Users: NewUserPostgres(db),
		Fails: NewFilePostgres(db)}
}
