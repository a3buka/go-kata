package repository

import (
	"database/sql"
	"errors"
	"fmt"
	"os"

	"github.com/sirupsen/logrus"

	"gitlab.com/a3buka/go-kata/module4/webserver/http/homework/iternal/entity"
)

var ErrorNotFound = "данный файл не найден"

const (
	fileTable = "file"
)

type FilePostgres struct {
	db *sql.DB
}

func NewFilePostgres(db *sql.DB) *FilePostgres {
	return &FilePostgres{db: db}
}

func (f *FilePostgres) CreateFile(fileName string) error {
	file, err := f.FindFile(fileName)
	if file.File != "" {
		return errors.New("файл с таким названием уже есть в директории")
	}

	query := fmt.Sprintf("INSERT INTO %s (filename) VALUES ($1) RETURNING id", fileTable)
	_, err = f.db.Query(query, fileName)
	if err != nil {
		return err
	}
	logrus.Info("файл успешно добавлен")

	/// создание файла в директории /public
	fileCreate, err := os.Create("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/http/homework/public/" + fileName)
	defer fileCreate.Close()
	if err != nil {
		errors.New("не удалось создать файл")
	}
	return nil //////////// Надо еще добавить файл в директорию
}

func (f *FilePostgres) FindFile(fileName string) (entity.File, error) {
	rows, err := f.db.Query("SELECT fileName FROM file")
	if err != nil {
		return entity.File{}, errors.New("request does not work at the stage of accessing the database")
	}
	var fileScan string
	var file entity.File
	for rows.Next() {
		err := rows.Scan(&fileScan)
		if err != nil {
			return entity.File{}, errors.New("ошибка скана на этапе считывания с база данных")
		}
		if fileScan == fileName {
			file = entity.File{File: fileScan}
			return file, nil
		}
	}
	return entity.File{}, errors.New(ErrorNotFound)
}
