package repository

import (
	"database/sql"
	"errors"

	"gitlab.com/a3buka/go-kata/module4/webserver/http/homework/iternal/entity"
)

type UserPostgres struct {
	db *sql.DB
}

func NewUserPostgres(db *sql.DB) *UserPostgres {
	return &UserPostgres{db: db}
}

func (repo *UserPostgres) GetAll() ([]entity.User, error) {
	rows, err := repo.db.Query("SELECT id, firstname, lastname FROM users")
	if err != nil {
		return nil, errors.New("request does not work at the stage of accessing the database")
	}
	var id int
	var firstname, lastname string
	UserDb := make([]entity.User, 0)
	for rows.Next() {
		err := rows.Scan(&id, &firstname, &lastname)
		if err != nil {
			return nil, errors.New("Ошибка скана на этапе считывания с база данных")
		}
		UserDb = append(UserDb, entity.User{ID: id, Firstname: firstname, Lastname: lastname})
	}
	return UserDb, nil
}

func (repo *UserPostgres) GetUserID(idSearch int) (entity.User, error) {
	rows, err := repo.db.Query("SELECT id, firstname, lastname FROM users")
	if err != nil {
		return entity.User{}, errors.New("request does not work at the stage of accessing the database")
	}
	var id int
	var firstname, lastname string
	var User entity.User
	for rows.Next() {
		err := rows.Scan(&id, &firstname, &lastname)
		if err != nil {
			return entity.User{}, errors.New("Ошибка скана на этапе считывания с база данных")
		}
		if idSearch == id {
			User = entity.User{ID: id, Firstname: firstname, Lastname: lastname}
			return User, nil
		}
	}
	return entity.User{}, errors.New("пользователь с заданным id не найден")

}
