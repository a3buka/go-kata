package service

import (
	"gitlab.com/a3buka/go-kata/module4/webserver/http/homework/iternal/entity"
)

type FailsService struct {
	repository FailsRepo
}

func NewFailsService(repo FailsRepo) *FailsService {
	return &FailsService{repository: repo}
}

func (f *FailsService) FindFile(fileName string) (entity.File, error) {
	return f.repository.FindFile(fileName)
}

func (f *FailsService) CreateFile(fileName string) error {
	return f.repository.CreateFile(fileName)
}
