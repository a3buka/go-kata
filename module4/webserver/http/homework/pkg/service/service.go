package service

import (
	"gitlab.com/a3buka/go-kata/module4/webserver/http/homework/iternal/entity"
)

type Repo interface {
	GetAll() ([]entity.User, error)
	GetUserID(id int) (entity.User, error)
	CreateFile(fileName string) error
	FindFile(fileName string) (entity.File, error)
}

type UsersRepo interface {
	GetAll() ([]entity.User, error)
	GetUserID(id int) (entity.User, error)
}

type FailsRepo interface {
	CreateFile(fileName string) error
	FindFile(fileName string) (entity.File, error)
}

type Service struct {
	UsersRepo
	FailsRepo
}

func NewService(repos Repo) *Service {
	return &Service{UsersRepo: NewUsersService(repos),
		FailsRepo: NewFailsService(repos)}
}
