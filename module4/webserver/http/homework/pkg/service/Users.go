package service

import (
	"gitlab.com/a3buka/go-kata/module4/webserver/http/homework/iternal/entity"
)

type UsersService struct {
	users UsersRepo
}

func NewUsersService(users UsersRepo) *UsersService {
	return &UsersService{users: users}
}

func (s *UsersService) GetAll() ([]entity.User, error) {
	return s.users.GetAll()
}

func (s *UsersService) GetUserID(id int) (entity.User, error) {
	return s.users.GetUserID(id)
}
