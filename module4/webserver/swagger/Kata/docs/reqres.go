package docs

import "gitlab.com/a3buka/go-kata/module4/webserver/swagger/internal/entity"

//go:generate swagger generate spec -o ./public/swagger.json --scan-models

// swagger:route POST /pet pet petAddRequest
// Добавление питомца.
// responses:
//   200: petAddResponse

// swagger:parameters petAddRequest
type petAddRequest struct {
	// in:body
	Body entity.Pet
}

// swagger:response petAddResponse
type petAddResponse struct {
	// in:body
	Body entity.Pet
}

// swagger:route GET /pet/{id} pet petGetByIDRequest
// Получение питомца по id.
// responses:
//   200: petGetByIDResponse

// swagger:parameters petGetByIDRequest
type petGetByIDRequest struct {
	// ID of an item
	//
	// In: path
	ID string `json:"id"`
}

// swagger:response petGetByIDResponse
type petGetByIDResponse struct {
	// in:body
	Body entity.Pet
}
