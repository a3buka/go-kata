package test

import (
	"reflect"
	"sync"
	"testing"

	"gitlab.com/a3buka/go-kata/module4/webserver/swagger/internal/repo"

	"gitlab.com/a3buka/go-kata/module4/webserver/swagger/internal/entity"
)

func TestPetStorage_Create(t *testing.T) {
	type fields struct {
		data               []*entity.Pet
		primaryKeyIDx      map[int64]*entity.Pet
		autoIncrementCount int64
		Mutex              sync.Mutex
	}
	type args struct {
		pet entity.Pet
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   entity.Pet
	}{
		{
			name: "first test",
			args: args{
				pet: entity.Pet{
					ID: 0,
					Category: entity.Category{
						ID:   0,
						Name: "test category",
					},
					Name: "Alma",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags:   []entity.Category{},
					Status: "active",
				},
			},
			want: entity.Pet{
				ID: 0,
				Category: entity.Category{
					ID:   0,
					Name: "test category",
				},
				Name: "Alma",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags:   []entity.Category{},
				Status: "active",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := repo.NewPetStorage()
			var got entity.Pet
			var err error
			got = p.Create(tt.args.pet)
			tt.want.ID = got.ID // fix autoincrement value
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
			if got, err = p.GetID(got.ID); err != nil || !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v, err %s", got, tt.want, err)
			}
		})
	}
}
