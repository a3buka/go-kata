package repo

import (
	"fmt"
	"sync"

	"gitlab.com/a3buka/go-kata/module4/webserver/swagger/internal/entity"
)

type PetSrorager interface {
	Create(pet entity.Pet) entity.Pet
	Update(pet entity.Pet) (entity.Pet, error)
	Delete(petID int) error
	GetID(petIDc int) (entity.Pet, error)
	GetList() []entity.Pet
}

type PetStorage struct {
	data               []*entity.Pet
	primaryKeyIDx      map[int64]*entity.Pet
	autoIncrementCount int64
	sync.Mutex
}

func NewPetStorage() *PetStorage {
	return &PetStorage{
		data:               make([]*entity.Pet, 0, 13),
		primaryKeyIDx:      make(map[int64]*entity.Pet, 13),
		autoIncrementCount: 1,
	}
}

func (p *PetStorage) Create(pet entity.Pet) entity.Pet {
	p.Lock()
	defer p.Unlock()
	pet.ID = p.autoIncrementCount
	p.primaryKeyIDx[pet.ID] = &pet
	p.autoIncrementCount++
	p.data = append(p.data, &pet)
	return pet
}

func (p *PetStorage) Update(pet entity.Pet) (entity.Pet, error) {
	//TODO implement me
	panic("implement me")
}

func (p *PetStorage) Delete(petID int) error {
	//TODO implement me
	panic("implement me")
}

func (p *PetStorage) GetID(petID int64) (entity.Pet, error) {
	if v, ok := p.primaryKeyIDx[petID]; ok {
		return *v, nil
	}
	return entity.Pet{}, fmt.Errorf("not found")
}

func (p *PetStorage) GetList() []entity.Pet {
	//TODO implement me
	panic("implement me")
}
