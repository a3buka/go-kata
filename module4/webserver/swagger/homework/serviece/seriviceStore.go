package serviece

import "gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/internal/entity"

type StoreService struct {
	storeServ StoreServicer
}

func NewStoreService(servicer StoreServicer) *StoreService {
	return &StoreService{storeServ: servicer}
}

func (s StoreService) Create(store entity.Store) entity.Store {
	return s.storeServ.Create(store)
}

func (s StoreService) GetID(storeID int) (entity.Store, error) {
	return s.storeServ.GetID(storeID)
}

func (s StoreService) Delete(storeID int) error {
	return s.storeServ.Delete(storeID)
}

func (s StoreService) GetAll() map[string]int {
	return s.storeServ.GetAll()
}
