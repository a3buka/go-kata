package serviece

import (
	"gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/internal/entity"
)

type petService struct {
	petServ PetServicer
}

func (p *petService) UploadImage(petID int, fileName string) error {
	return p.petServ.UploadImage(petID, fileName)
}

func NewPetService(servicer PetServicer) *petService {
	return &petService{petServ: servicer}
}

func (p *petService) Create(pet entity.Pet) entity.Pet {
	return p.petServ.Create(pet)
}

func (p *petService) Update(pet entity.Pet) (entity.Pet, error) {
	return p.petServ.Update(pet)
}

func (p *petService) Delete(petID int) error {
	return p.petServ.Delete(petID)
}

func (p *petService) GetID(petIDc int) (entity.Pet, error) {
	return p.petServ.GetID(petIDc)
}

func (p *petService) FindByStatus(status string) ([]*entity.Pet, error) {
	return p.petServ.FindByStatus(status)
}

func (p *petService) UpdatePOST(petID int, name, status string) error {
	return p.petServ.UpdatePOST(petID, name, status)
}

//func (p petService) GetList() []entity.Pet {
//	return p.petServ.GetList()
//}
