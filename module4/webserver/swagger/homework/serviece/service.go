package serviece

import (
	"gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/internal/entity"
)

type UserServiecer interface {
	Create(user entity.User) (entity.User, error)
	GetUsername(userName string) (entity.User, error)
	Delete(userName string) error
	Update(userName string, user entity.User) (entity.User, error)
	CreateWithArray(user []entity.User) []entity.User
}

type PetServicer interface {
	Create(pet entity.Pet) entity.Pet
	Update(pet entity.Pet) (entity.Pet, error)
	Delete(petID int) error
	GetID(petIDc int) (entity.Pet, error)
	FindByStatus(status string) ([]*entity.Pet, error)
	UpdatePOST(petID int, name, status string) error
	UploadImage(petID int, fileName string) error
	//GetList() []entity.Pet
}

type StoreServicer interface {
	Create(store entity.Store) entity.Store
	GetID(storeID int) (entity.Store, error)
	Delete(storeID int) error
	GetAll() map[string]int
}

type Service struct {
	UserServiecer
	PetServicer
	StoreServicer
}

func NewService(user UserServiecer, pet PetServicer, store StoreServicer) *Service {
	return &Service{
		UserServiecer: NewUserService(user),
		PetServicer:   NewPetService(pet),
		StoreServicer: NewStoreService(store),
	}
}
