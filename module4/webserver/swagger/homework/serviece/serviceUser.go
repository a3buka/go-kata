package serviece

import "gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/internal/entity"

type userService struct {
	userServ UserServiecer
}

func NewUserService(servicer UserServiecer) *userService {
	return &userService{userServ: servicer}
}

func (u userService) Create(user entity.User) (entity.User, error) {
	return u.userServ.Create(user)
}

func (u userService) GetUsername(userName string) (entity.User, error) {
	return u.userServ.GetUsername(userName)
}

func (u userService) Delete(userName string) error {
	return u.userServ.Delete(userName)
}

func (u userService) Update(userName string, user entity.User) (entity.User, error) {
	return u.userServ.Update(userName, user)
}

func (u userService) CreateWithArray(user []entity.User) []entity.User {
	return u.userServ.CreateWithArray(user)
}
