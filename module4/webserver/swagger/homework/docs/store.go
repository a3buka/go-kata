package docs

import "gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/internal/entity"

//go:generate swagger generate spec -o ./public/swagger.json --scan-models

// swagger:route POST /store/order store orderCreateRequest
// Создание ордера для питомца.
// responses:
// 200: CreateResponse
// 400: description: Invalid order

// swagger:parameters orderCreateRequest
type orderCreateRequest struct {
	// Создание ордера для питомца
	// required:true
	// in:body
	Store entity.Store
}

// swagger:response CreateResponse
type CreateResponse struct {
	//in:body
	Store entity.Store
}

// swagger:route GET /store/order/{id} store orderGetRequest
// Получение ордера по id.
// responses:
//   200: petGetByIDResponse
//	 400: description: Bad request
//	 500: description: Internal server Error

// swagger:parameters orderGetRequest
type orderGetRequest struct {
	// ID питомца которого необходимо найти
	// required:true
	// in:path
	Id int64 `json:"id"`
}

// swagger:response orderGetResponse
type orderGetResponse struct {
	// in:body
	Store entity.Store
}

// swagger:route DELETE /store/{id} store orderDeleteRequest
// Удаление ордера.
// responses:
//	200: description: successfully
//	400: description: Bad request
//	500: description: Internal server error

// swagger:parameters orderDeleteRequest
type orderDeleteRequest struct {
	// id ордера
	// required:true
	// in:path
	Id int64 `json:"id"`
}

// swagger:route GET /store/inventory store storeGetAllRequest
// Получение всех ордеров.
// responses:
//
//	200: storeGetAllResponse
//	500: description: Internal server error

// swagger:parameters storeGetAllRequest

// swagger:response storeGetAllResponse
type storeGetAllResponse struct {
	// in:body
	Body []entity.Store
}
