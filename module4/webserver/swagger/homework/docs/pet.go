package docs

import (
	"bytes"

	"gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/internal/entity"
)

//go:generate swagger generate spec -o ./public/swagger.json --scan-models

// swagger:route POST /pet/{petID}/uploadImage pet petUploadImageRequest
// Добавление картинки питомца по ID.
// responses:
//	200: description: successfully
//	500: description: Internal server error

// swagger:parameters petUploadImageRequest
type petUploadImageRequest struct {
	// ID of pet
	// required: true
	// in:path
	PetID int64 `json:"petID"`

	// file to upload
	// in: formData
	// swagger:file
	File *bytes.Buffer `json:"file"`
}

// swagger:route POST /pet pet petAddRequest
// Добавление питомца.
// responses:
//   200: petAddResponse
//	 400: description: Bad request
//	 500: description: Internal server Error

// swagger:parameters petAddRequest
type petAddRequest struct {
	// Create pet with ID
	// required:true
	// in:body
	Body entity.Pet
}

// swagger:response petAddResponse
type petAddResponse struct {
	// in:body
	Body entity.Pet
}

// swagger:route PUT /pet/ pet petPutById
// Обновление питомца с необходимым ID.
// responses:
//   200: petUpdateResponse
//	 400: description: Bad request
//	 500: description: Internal server Error

// swagger:parameters petPutById
type petPutById struct {
	// Pet id to be updated
	// required:true
	// in:body
	Body entity.Pet
}

// swagger:response petUpdateResponse
type petUpdateResponse struct {
	// in:body
	Body entity.Pet
}

// swagger:route GET /pet/findGetByStatus pet petGetByStatusRequest
// Получение питомца по доступному статусу.
// responses:
//   200: petGetByStatusResponse
//	 400: description: Bad request
//	 500: description: Internal server Error

// swagger:parameters petGetByStatusRequest
type petGetByStatusRequest struct {
	// Status of Pet
	// required:true
	// items.enum:available,pending,sold
	// in:query
	Status []string `json:"status"`
}

// swagger:response petGetByStatusResponse
type petGetByStatusResponse struct {
	// in:body
	Body []entity.Pet
}

// swagger:route GET /pet/{petID} pet petGetByIDRequest
// Получение питомца по id.
// responses:
//   200: petGetByIDResponse
//	 400: description: Bad request
//	 500: description: Internal server Error

// swagger:parameters petGetByIDRequest
type petGetByIDRequest struct {
	// ID of an item
	// required:true
	// in:path
	PetID int64 `json:"petID"`
}

// swagger:response petGetByIDResponse
type petGetByIDResponse struct {
	// in:body
	Body entity.Pet
}

// swagger:route POST /pet/{petID} pet petUpdate
// Обновления питомца с помощью значений.
// responses:
//	405: description: invalid input

// swagger:parameters petUpdate
type petUpdateRequest struct {
	// ID of pet that needs to be updated
	// required:true
	// in:path
	PetID int64 `json:"petID"`

	// Updated name of the pet
	// in:formData
	Name string `json:"name"`

	// Updated status of the pet
	// in:formData
	Status string `json:"status"`
}

// swagger:route DELETE /pet/{petID} pet petDeleteRequest
// Удаление питомца.
// responses:
//	200: description: successfully
//	400: description: Bad request
//	500: description: Internal server error

// swagger:parameters petDeleteRequest
type petDeleteRequest struct {
	// ID of pet
	// required: true
	// in:path
	PetID int64 `json:"petID"`
}
