package docs

import "gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/internal/entity"

//go:generate swagger generate spec -o ./public/swagger.json --scan-models

// swagger:route POST /user user userCreateRequest
// Добавление нового пользователя.
// responses:
//  200: userCreateResponse

// swagger:parameters userCreateRequest
type userCreateRequest struct {
	// Created user object
	// required:true
	// in:body
	User entity.User
}

// swagger:response userCreateResponse
type userCreateResponse struct {
	// in:body
	User entity.User
}

//swagger:route POST /user/createWithArray user userCreateWithArrayRequest
// Добавления нового пользователя с помощью массива.
// responses:
//  200: userCreateWithArrayResponse
//  400: description: Bad request
//  409: description: User exists
//	500: description: Internal server error

// swagger:parameters userCreateWithArrayRequest
type userCreateWithArrayRequest struct {
	// List of user object
	// required:true
	// in:body
	User entity.User
}

// swagger:response userCreateWithArrayResponse
type userCreateWithArrayResponse struct {
	// in:body
	User entity.User
}

//swagger:route GET /user/{username} user userGetWithUsernameRequest
// Получения юзера по его username.
// responses:
// 200: userGetWithUsernameResponse
// 400: description: Invalid username supplied
// 404: description: User not found

// swagger:parameters userGetWithUsernameRequest
type userGetWithUsernameRequest struct {
	// The name that needs to be fetched
	// required:true
	// in:path
	Username string `json:"username"`
}

// swagger:response userGetWithUsernameResponse
type userGetWithUsernameResponse struct {
	// in:body
	User entity.User
}

//swagger:route PUT /user/{username} user userUpdateWithUsernameRequest
// Обновление юзера по его username.
// responses:
// 200: userUpdateWithUsernameResponse
// 400: description: Invalid username supplied
// 404: description: User not found

// swagger:parameters userUpdateWithUsernameRequest
type userUpdateWithUsernameRequest struct {
	// Name that needed to update
	// required:true
	// in:path
	Username string `json:"username"`

	// Update user object
	// in:body
	User entity.User
}

// swagger:response userUpdateWithUsernameResponse
type userUpdateWithUsernameResponse struct {
	// in:body
	User entity.User
}

//swagger:route DELETE /user/{username} user userDeleteWithUsernameRequest
// Удаление пользователя по его username.
// responses:
// 200: description: successfully
// 400: description: Invalid username supplied
// 404: description: User not found

// swagger:parameters userDeleteWithUsernameRequest
type userDeleteWithUsernameRequest struct {
	// Name that needs to be deleted
	// required:true
	// in:path
	Username string `json:"username"`
}
