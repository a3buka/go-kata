package repository

import (
	"encoding/json"
	"fmt"
	"os"
	"sync"

	"gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/internal/entity"
)

type StoreStorage struct {
	data               []*entity.Store
	primaryKeyIDx      map[int64]*entity.Store
	autoIncrementCount int64
	file               string
	sync.Mutex
}

func NewStoreStorage(fileName string) *StoreStorage {
	body, err := os.ReadFile(fileName)
	if err != nil {
		return &StoreStorage{
			data:               make([]*entity.Store, 0, 13),
			primaryKeyIDx:      make(map[int64]*entity.Store, 13),
			autoIncrementCount: 1,
			file:               fileName,
		}
	}
	var orders []entity.Store

	err = json.Unmarshal(body, &orders)
	if err != nil {
		return &StoreStorage{
			data:               make([]*entity.Store, 0, 13),
			primaryKeyIDx:      make(map[int64]*entity.Store, 13),
			autoIncrementCount: 1,
			file:               fileName,
		}
	}

	dataStorage := make([]*entity.Store, 0, 13)
	primaryKeyStorage := make(map[int64]*entity.Store, 13)
	var a int64
	for i := range orders {
		dataStorage = append(dataStorage, &orders[i])
		primaryKeyStorage[orders[i].ID] = &orders[i]
		if orders[i].ID > a {
			a = orders[i].ID
		}
	}
	a++
	return &StoreStorage{
		file:               fileName,
		data:               dataStorage,
		primaryKeyIDx:      primaryKeyStorage,
		autoIncrementCount: a}

}

func (s *StoreStorage) Create(store entity.Store) entity.Store {
	s.Lock()
	defer s.Unlock()
	store.ID = s.autoIncrementCount
	if store.Status == "string" {
		store.Status = "available"
	}
	s.primaryKeyIDx[store.ID] = &store
	s.autoIncrementCount++
	s.data = append(s.data, &store)

	data, _ := json.MarshalIndent(s.data, "", "\t")
	_ = WriteInFile(s.file, data)

	return store
}

func (s *StoreStorage) GetID(storeID int) (entity.Store, error) {
	if v, ok := s.primaryKeyIDx[int64(storeID)]; ok {
		return *v, nil
	}
	return entity.Store{}, fmt.Errorf("not found")
}

func (s *StoreStorage) Delete(storeID int) error {
	s.Lock()
	defer s.Unlock()
	if v, ok := s.primaryKeyIDx[int64(storeID)]; ok {
		delete(s.primaryKeyIDx, v.ID)
		for i := range s.data {
			if s.data[i] == v {
				s.data = append(s.data[:i], s.data[i+1:]...)
				break
			}
		}
		data, _ := json.MarshalIndent(s.data, "", "\t")
		_ = WriteInFile(s.file, data)
		return nil
	}
	return fmt.Errorf("index not found")
}

func (s *StoreStorage) GetAll() map[string]int {
	s.Lock()
	defer s.Unlock()

	result := make(map[string]int, 10) //предположим что количество различных статусов ровно 10
	for _, item := range s.data {
		result[item.Status]++
	}
	return result

}
