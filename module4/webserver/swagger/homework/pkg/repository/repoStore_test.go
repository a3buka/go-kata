package repository

import (
	"fmt"
	"os"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/internal/entity"
)

var (
	storeTest1 = entity.Store{ID: 1, PetID: 1, Status: "available"}
	storeTest2 = entity.Store{ID: 2, PetID: 2, Status: "sold"}
	storeTest3 = entity.Store{ID: 3, PetID: 2, Status: "available"}
)

func TestStoreStorage_Create(t *testing.T) {
	s := NewStoreStorage("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/store_test.json")
	defer os.Remove("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/store_test.json")
	type fields struct {
		data               []*entity.Store
		primaryKeyIDx      map[int64]*entity.Store
		autoIncrementCount int64
		file               string
		Mutex              sync.Mutex
	}
	type args struct {
		store entity.Store
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   entity.Store
	}{
		{
			name: "test create store 1",
			args: args{storeTest1},
			want: storeTest1,
		},
		{
			name: "test create store 2",
			args: args{storeTest2},
			want: storeTest2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, s.Create(tt.args.store), "Create(%v)", tt.args.store)
		})
	}
}

func TestStoreStorage_GetID(t *testing.T) {
	s := NewStoreStorage("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/store_test.json")
	s.Create(storeTest1)
	s.Create(storeTest3)
	s.Create(storeTest3)
	defer os.Remove("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/store_test.json")
	type fields struct {
		data               []*entity.Store
		primaryKeyIDx      map[int64]*entity.Store
		autoIncrementCount int64
		file               string
		Mutex              sync.Mutex
	}
	type args struct {
		storeID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entity.Store
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "test store get 1",
			args: args{1},
			want: storeTest1,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return true
			},
		},
		{
			name: "test store get 1",
			args: args{5},
			want: entity.Store{},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return false
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := s.GetID(tt.args.storeID)
			if !tt.wantErr(t, err, fmt.Sprintf("GetID(%v)", tt.args.storeID)) {
				return
			}
			assert.Equalf(t, tt.want, got, "GetID(%v)", tt.args.storeID)
		})
	}
}

func TestStoreStorage_Delete(t *testing.T) {
	s := NewStoreStorage("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/store_test.json")
	s.Create(storeTest1)
	s.Create(storeTest3)
	s.Create(storeTest3)
	defer os.Remove("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/store_test.json")
	type fields struct {
		data               []*entity.Store
		primaryKeyIDx      map[int64]*entity.Store
		autoIncrementCount int64
		file               string
		Mutex              sync.Mutex
	}
	type args struct {
		storeID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "test store del 1",
			args: args{1},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return true
			},
		},
		{
			name: "test store del 1",
			args: args{1},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return false
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.wantErr(t, s.Delete(tt.args.storeID), fmt.Sprintf("Delete(%v)", tt.args.storeID))
		})
	}
}

func TestStoreStorage_GetAll(t *testing.T) {
	s := NewStoreStorage("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/store_test.json")
	s.Create(storeTest1)
	s.Create(storeTest2)
	s.Create(storeTest3)
	defer os.Remove("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/store_test.json")

	type fields struct {
		data               []*entity.Store
		primaryKeyIDx      map[int64]*entity.Store
		autoIncrementCount int64
		file               string
		Mutex              sync.Mutex
	}
	tests := []struct {
		name   string
		fields fields
		want   map[string]int
	}{
		{
			name: "test get ALL1",
			want: map[string]int{
				"available": 2,
				"sold":      1,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, s.GetAll(), "GetAll()")
		})
	}
}
