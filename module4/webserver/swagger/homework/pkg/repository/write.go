package repository

import "os"

func WriteInFile(s string, d []byte) error {
	if err := os.WriteFile(s, d, 0644); err != nil {
		return err
	}
	return nil

}
