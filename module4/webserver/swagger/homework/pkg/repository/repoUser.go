package repository

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"sync"

	"gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/internal/entity"
)

type UserStorage struct {
	data               []*entity.User
	primaryKeyIDx      map[string]*entity.User
	autoIncrementCount int64
	file               string
	sync.Mutex
}

func NewUserStorage(fileName string) *UserStorage {
	body, err := os.ReadFile(fileName)
	if err != nil {
		return &UserStorage{
			data:               make([]*entity.User, 0, 13),
			primaryKeyIDx:      make(map[string]*entity.User, 13),
			autoIncrementCount: 1,
			file:               fileName,
		}
	}
	var user []entity.User
	err = json.Unmarshal(body, &user)
	if err != nil {
		return &UserStorage{
			data:               make([]*entity.User, 0, 13),
			primaryKeyIDx:      make(map[string]*entity.User, 13),
			autoIncrementCount: 1,
			file:               fileName,
		}
	}
	dataStorage := make([]*entity.User, 0, 13)
	primaryKeyStorage := make(map[string]*entity.User, 13)
	var a int64
	for i := range user {
		dataStorage = append(dataStorage, &user[i])
		primaryKeyStorage[user[i].Username] = &user[i]
		if user[i].ID > a {
			a = user[i].ID
		}
	}
	a++
	return &UserStorage{
		file:               fileName,
		data:               dataStorage,
		primaryKeyIDx:      primaryKeyStorage,
		autoIncrementCount: a}
}

func (u *UserStorage) Create(user entity.User) (entity.User, error) {
	u.Lock()
	defer u.Unlock()
	if _, ok := u.primaryKeyIDx[user.Username]; ok {
		return entity.User{}, errors.New("юзер с таким username уже существует")
	}
	user.ID = u.autoIncrementCount
	u.primaryKeyIDx[user.Username] = &user
	u.autoIncrementCount++
	u.data = append(u.data, &user)

	data, _ := json.MarshalIndent(u.data, "", "\t")
	_ = WriteInFile(u.file, data)
	return user, nil

}

func (u *UserStorage) GetUsername(userName string) (entity.User, error) {
	if user, ok := u.primaryKeyIDx[userName]; ok {
		return *user, nil
	}
	return entity.User{}, errors.New("invalid username")
}

func (u *UserStorage) Delete(userName string) error {
	u.Lock()
	defer u.Unlock()
	if v, ok := u.primaryKeyIDx[userName]; ok {
		delete(u.primaryKeyIDx, v.Username)
		for i := range u.data {
			if u.data[i] == v {
				u.data = append(u.data[:i], u.data[i+1:]...)
				break
			}
		}
		data, _ := json.MarshalIndent(u.data, "", "\t")
		_ = WriteInFile(u.file, data)
		return nil
	}
	return fmt.Errorf("index not found")
}

func (u *UserStorage) Update(userName string, user entity.User) (entity.User, error) {
	u.Lock()
	defer u.Unlock()

	if _, ok := u.primaryKeyIDx[userName]; ok {
		u.primaryKeyIDx[userName] = &user
		for i := range u.data {
			if u.data[i].Username == user.Username {
				u.data[i] = &user
				break
			}
		}
		data, _ := json.MarshalIndent(u.data, "", "\t")
		_ = WriteInFile(u.file, data)
		return user, nil
	}
	return entity.User{}, fmt.Errorf("user not found")
}

func (u *UserStorage) CreateWithArray(user []entity.User) []entity.User {
	for i := range user {
		if _, ok := u.primaryKeyIDx[user[i].Username]; ok {
			continue
		}
		user[i].ID = u.autoIncrementCount
		u.primaryKeyIDx[user[i].Username] = &user[i]
		u.autoIncrementCount++
		u.data = append(u.data, &user[i])
	}
	data, _ := json.MarshalIndent(u.data, "", "\t")
	_ = WriteInFile(u.file, data)
	return user
}
