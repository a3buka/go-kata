package repository

import (
	"fmt"

	"os"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/internal/entity"
)

var (
	testUser1 = entity.User{
		Username:   "abc",
		ID:         1,
		FirstName:  "evg",
		UserStatus: 1,
	}

	testUser2 = entity.User{
		Username:   "korm",
		ID:         2,
		FirstName:  "john",
		UserStatus: 1,
	}

	testUser3 = entity.User{
		Username:   "klerk",
		ID:         3,
		FirstName:  "graal",
		UserStatus: 4,
	}
)

func TestUserStorage_Create(t *testing.T) {
	s := NewUserStorage("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/user_test.json")
	defer os.Remove("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/user_test.json")
	type fields struct {
		data               []*entity.User
		primaryKeyIDx      map[string]*entity.User
		autoIncrementCount int64
		file               string
		Mutex              sync.Mutex
	}
	type args struct {
		user entity.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entity.User
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "test create 1",
			args: args{testUser1},
			want: testUser1,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return true
			},
		},
		{
			name: "test create 2",
			args: args{testUser2},
			want: testUser2,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return true
			},
		},
		{
			name: "test create 3",
			args: args{testUser2},
			want: testUser3,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return false
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := s.Create(tt.args.user)
			if !tt.wantErr(t, err, fmt.Sprintf("Create(%v)", tt.args.user)) {
				return
			}
			assert.Equalf(t, tt.want, got, "Create(%v)", tt.args.user)
		})
	}
}

func TestUserStorage_GetUsername(t *testing.T) {
	s := NewUserStorage("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/user_test.json")
	s.Create(testUser1)
	s.Create(testUser2)
	s.Create(testUser3)
	defer os.Remove("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/user_test.json")
	type fields struct {
		data               []*entity.User
		primaryKeyIDx      map[string]*entity.User
		autoIncrementCount int64
		file               string
		Mutex              sync.Mutex
	}
	type args struct {
		userName string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entity.User
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "test get username 1",
			args: args{"korm"},
			want: testUser2,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return true
			},
		},
		{
			name: "test get username 2",
			args: args{"kormil"},
			want: testUser1,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return false
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := s.GetUsername(tt.args.userName)
			if !tt.wantErr(t, err, fmt.Sprintf("GetUsername(%v)", tt.args.userName)) {
				return
			}
			assert.Equalf(t, tt.want, got, "GetUsername(%v)", tt.args.userName)
		})
	}
}

func TestUserStorage_Delete(t *testing.T) {
	s := NewUserStorage("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/user_test.json")
	s.Create(testUser1)
	s.Create(testUser2)
	s.Create(testUser3)
	defer os.Remove("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/user_test.json")
	type fields struct {
		data               []*entity.User
		primaryKeyIDx      map[string]*entity.User
		autoIncrementCount int64
		file               string
		Mutex              sync.Mutex
	}
	type args struct {
		userName string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "test 1",
			args: args{"korm"},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return true
			},
		},
		{
			name: "test 1",
			args: args{"korm"},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return false
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			tt.wantErr(t, s.Delete(tt.args.userName), fmt.Sprintf("Delete(%v)", tt.args.userName))
		})
	}
}

func TestUserStorage_Update(t *testing.T) {
	s := NewUserStorage("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/user_test.json")
	s.Create(testUser1)
	s.Create(testUser2)
	s.Create(testUser3)
	defer os.Remove("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/user_test.json")
	type fields struct {
		data               []*entity.User
		primaryKeyIDx      map[string]*entity.User
		autoIncrementCount int64
		file               string
		Mutex              sync.Mutex
	}
	type args struct {
		userName string
		user     entity.User
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entity.User
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "test update 1",
			args: args{"korm", entity.User{Username: "bra", ID: 5, Email: "brik"}},
			want: entity.User{Username: "bra", ID: 5, Email: "brik"},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return true
			},
		},
		{
			name: "test update 2",
			args: args{"krek", entity.User{Username: "bra", ID: 5, Email: "brik"}},
			want: entity.User{},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return false
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := s.Update(tt.args.userName, tt.args.user)
			if !tt.wantErr(t, err, fmt.Sprintf("Update(%v, %v)", tt.args.userName, tt.args.user)) {
				return
			}
			assert.Equalf(t, tt.want, got, "Update(%v, %v)", tt.args.userName, tt.args.user)
		})
	}
}

func TestUserStorage_CreateWithArray(t *testing.T) {
	s := NewUserStorage("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/user_test.json")
	defer os.Remove("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/user_test.json")
	type fields struct {
		data               []*entity.User
		primaryKeyIDx      map[string]*entity.User
		autoIncrementCount int64
		file               string
		Mutex              sync.Mutex
	}
	type args struct {
		user []entity.User
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   []entity.User
	}{
		{
			name: "test user array 1",
			args: args{[]entity.User{testUser1, testUser2, testUser3}},
			want: []entity.User{testUser1, testUser2, testUser3},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, s.CreateWithArray(tt.args.user), "CreateWithArray(%v)", tt.args.user)
		})
	}
}
