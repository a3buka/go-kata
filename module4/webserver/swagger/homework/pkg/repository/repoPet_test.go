package repository

import (
	"fmt"
	"os"
	"sync"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/internal/entity"
)

var (
	testPet1 = entity.Pet{
		ID: 1,
		Category: entity.Category{
			ID:   1,
			Name: "testPet1",
		},
		Name: "Evg",
		PhotoUrls: []string{
			"string",
		},
		Tags:   []entity.Category{},
		Status: "available",
	}

	testPet2 = entity.Pet{
		ID: 2,
		Category: entity.Category{
			ID:   2,
			Name: "testPet2",
		},
		Name: "Kom",
		PhotoUrls: []string{
			"1.jpg",
			"2.jpg",
			"2.jpg",
		},
		Tags:   []entity.Category{},
		Status: "sold",
	}

	testPet3 = entity.Pet{
		ID: 3,
		Category: entity.Category{
			ID:   3,
			Name: "testPet3",
		},
		Name: "Bra",
		PhotoUrls: []string{
			"4.jpg",
			"5.jpg",
			"6.jpg",
		},
		Tags:   []entity.Category{},
		Status: "available",
	}
)

func TestPetStorage_Create(t *testing.T) {
	s := NewPetStorage("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/pets_test.json")
	defer os.Remove("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/pets_test.json")
	type fields struct {
		data               []*entity.Pet
		primaryKeyIDx      map[int64]*entity.Pet
		autoIncrementCount int64
		file               string
		Mutex              sync.Mutex
	}
	type args struct {
		pet entity.Pet
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   entity.Pet
	}{
		{
			name: "test1",
			args: args{testPet1},
			want: testPet1,
		},
		{
			name: "test2",
			args: args{testPet2},
			want: testPet2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assert.Equalf(t, tt.want, s.Create(tt.args.pet), "Create(%v)", tt.args.pet)
		})
	}
}

func TestPetStorage_Update(t *testing.T) {
	s := NewPetStorage("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/pets_test.json")
	defer os.Remove("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/pets_test.json")
	type fields struct {
		data               []*entity.Pet
		primaryKeyIDx      map[int64]*entity.Pet
		autoIncrementCount int64
		file               string
		Mutex              sync.Mutex
	}
	type args struct {
		pet entity.Pet
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entity.Pet
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "test update",
			args: args{testPet1},
			want: testPet1,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return false
			},
		},
		{
			name: "test update",
			args: args{testPet1},
			want: entity.Pet{},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return true
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			got, err := s.Update(tt.args.pet)
			if !tt.wantErr(t, err, fmt.Sprintf("Update(%v)", tt.args.pet)) {
				return
			}
			assert.Equalf(t, tt.want, got, "Update(%v)", tt.args.pet)
		})
	}
}

func TestPetStorage_FindByStatus(t *testing.T) {
	s := NewPetStorage("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/pets_test.json")
	s.Create(testPet1)
	s.Create(testPet2)
	s.Create(testPet3)
	defer os.Remove("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/pets_test.json")
	type fields struct {
		data               []*entity.Pet
		primaryKeyIDx      map[int64]*entity.Pet
		autoIncrementCount int64
		file               string
		Mutex              sync.Mutex
	}
	type args struct {
		status string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []*entity.Pet
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "test update",
			args: args{"available"},
			want: []*entity.Pet{&testPet1, &testPet3},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return true
			},
		},
		{
			name: "test update",
			args: args{"available"},
			want: []*entity.Pet{&testPet1, &testPet3, &testPet2},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return false
			},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			got, err := s.FindByStatus(tt.args.status)
			if !tt.wantErr(t, err, fmt.Sprintf("FindByStatus(%v)", tt.args.status)) {
				return
			}
			assert.Equalf(t, tt.want, got, "FindByStatus(%v)", tt.args.status)
		})
	}
}

func TestPetStorage_Delete(t *testing.T) {
	s := NewPetStorage("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/pets_test.json")
	s.Create(testPet1)
	s.Create(testPet2)
	s.Create(testPet3)
	defer os.Remove("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/pets_test.json")
	type fields struct {
		data               []*entity.Pet
		primaryKeyIDx      map[int64]*entity.Pet
		autoIncrementCount int64
		file               string
		Mutex              sync.Mutex
	}
	type args struct {
		petID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "test delete 1",
			args: args{1},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return true
			},
		},
		{
			name: "test delete 1",
			args: args{1},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return false
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			tt.wantErr(t, s.Delete(tt.args.petID), fmt.Sprintf("Delete(%v)", tt.args.petID))
		})
	}
}

func TestPetStorage_GetID(t *testing.T) {
	s := NewPetStorage("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/pets_test.json")
	s.Create(testPet1)
	s.Create(testPet2)
	s.Create(testPet3)
	defer os.Remove("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/pets_test.json")
	type fields struct {
		data               []*entity.Pet
		primaryKeyIDx      map[int64]*entity.Pet
		autoIncrementCount int64
		file               string
		Mutex              sync.Mutex
	}
	type args struct {
		petID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entity.Pet
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "test 1",
			args: args{1},
			want: testPet1,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return true
			},
		},
		{
			name: "test 2",
			args: args{5},
			want: entity.Pet{},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return true
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := s.GetID(tt.args.petID)
			if !tt.wantErr(t, err, fmt.Sprintf("GetID(%v)", tt.args.petID)) {
				return
			}
			assert.Equalf(t, tt.want, got, "GetID(%v)", tt.args.petID)
		})
	}
}

func TestPetStorage_UpdatePOST(t *testing.T) {
	s := NewPetStorage("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/pets_test.json")
	s.Create(testPet1)
	s.Create(testPet2)
	s.Create(testPet3)
	defer os.Remove("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/pets_test.json")
	type fields struct {
		data               []*entity.Pet
		primaryKeyIDx      map[int64]*entity.Pet
		autoIncrementCount int64
		file               string
		Mutex              sync.Mutex
	}
	type args struct {
		petID  int
		name   string
		status string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "test update1",
			args: args{petID: 1, name: "John", status: "sold"},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return true
			},
		},
		{
			name: "test update1",
			args: args{petID: 4, name: "John", status: "sold"},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return false
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			tt.wantErr(t, s.UpdatePOST(tt.args.petID, tt.args.name, tt.args.status), fmt.Sprintf("UpdatePOST(%v, %v, %v)", tt.args.petID, tt.args.name, tt.args.status))
		})
	}
}

func TestPetStorage_UploadImage(t *testing.T) {
	s := NewPetStorage("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/pets_test.json")
	s.Create(testPet1)
	s.Create(testPet2)
	s.Create(testPet3)
	defer os.Remove("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/db/pets_test.json")
	type fields struct {
		data               []*entity.Pet
		primaryKeyIDx      map[int64]*entity.Pet
		autoIncrementCount int64
		file               string
		Mutex              sync.Mutex
	}
	type args struct {
		petID    int
		fileName string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr assert.ErrorAssertionFunc
	}{
		{
			name: "test Upload1",
			args: args{petID: 1, fileName: "kok.jpg"},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return true
			},
		},
		{
			name: "test Upload2",
			args: args{petID: 1, fileName: "kok.txt"},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return false
			},
		},
		{
			name: "test Upload3",
			args: args{petID: 5, fileName: "kok.txt"},
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return false
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			tt.wantErr(t, s.UploadImage(tt.args.petID, tt.args.fileName), fmt.Sprintf("UploadImage(%v, %v)", tt.args.petID, tt.args.fileName))
		})
	}
}
