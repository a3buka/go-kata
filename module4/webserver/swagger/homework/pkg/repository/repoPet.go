package repository

import (
	"encoding/json"
	"fmt"
	"os"
	"sync"

	"gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/internal/entity"
)

type PetStorage struct {
	data               []*entity.Pet
	primaryKeyIDx      map[int64]*entity.Pet
	autoIncrementCount int64
	file               string
	sync.Mutex
}

func NewPetStorage(fileName string) *PetStorage {
	body, err := os.ReadFile(fileName)
	if err != nil {
		return &PetStorage{
			data:               make([]*entity.Pet, 0, 13),
			primaryKeyIDx:      make(map[int64]*entity.Pet, 13),
			autoIncrementCount: 1,
			file:               fileName,
		}
	}
	var pets []entity.Pet

	err = json.Unmarshal(body, &pets)
	if err != nil {
		return &PetStorage{
			data:               make([]*entity.Pet, 0, 13),
			primaryKeyIDx:      make(map[int64]*entity.Pet, 13),
			autoIncrementCount: 1,
			file:               fileName,
		}
	}

	dataStorage := make([]*entity.Pet, 0, 13)
	primaryKeyStorage := make(map[int64]*entity.Pet, 13)
	var a int64
	for i := range pets {
		dataStorage = append(dataStorage, &pets[i])
		primaryKeyStorage[pets[i].ID] = &pets[i]
		if pets[i].ID > a {
			a = pets[i].ID
		}
	}
	a++
	return &PetStorage{
		file:               fileName,
		data:               dataStorage,
		primaryKeyIDx:      primaryKeyStorage,
		autoIncrementCount: a}
}

func (p *PetStorage) UploadImage(petID int, fileName string) error {
	p.Lock()
	defer p.Unlock()
	//Добавляем данные о загруженном файле в структуру ПЕТ
	if _, ok := p.primaryKeyIDx[int64(petID)]; !ok {
		return fmt.Errorf("id not found")
	}

	if p.primaryKeyIDx[int64(petID)].PhotoUrls[0] == "string" {
		p.primaryKeyIDx[int64(petID)].PhotoUrls[0] = fileName
	} else {
		p.primaryKeyIDx[int64(petID)].PhotoUrls = append(p.primaryKeyIDx[int64(petID)].PhotoUrls, fileName)
	}

	data, _ := json.MarshalIndent(p.primaryKeyIDx, "", "\t")
	_ = WriteInFile(p.file, data)

	//Загружаем файл на сервер. Вот тут конечно вопросик куда в чистой архитектуре девать этот момент
	//f, err := os.OpenFile("./img/"+strconv.Itoa(petID)+fileName, os.O_WRONLY|os.O_CREATE, 0644)
	//if err != nil {
	//	fmt.Errorf("error file loading")
	//}
	//defer f.Close()
	//io.Copy(f, file)
	return nil
}

func (p *PetStorage) Create(pet entity.Pet) entity.Pet {
	p.Lock()
	defer p.Unlock()
	pet.ID = p.autoIncrementCount
	if pet.Status == "string" {
		pet.Status = "available"
	}

	p.primaryKeyIDx[pet.ID] = &pet
	p.autoIncrementCount++
	p.data = append(p.data, &pet)

	data, _ := json.MarshalIndent(p.data, "", "\t")
	_ = WriteInFile(p.file, data)

	return pet
}

func (p *PetStorage) Update(pet entity.Pet) (entity.Pet, error) {
	p.Lock()
	defer p.Unlock()

	if pet.ID < 1 {
		return entity.Pet{}, fmt.Errorf("invalid ID format")
	}

	if _, ok := p.primaryKeyIDx[pet.ID]; ok {
		p.primaryKeyIDx[pet.ID] = &pet
		for i := range p.data {
			if p.data[i].ID == pet.ID {
				p.data[i] = &pet
				break
			}
		}
		data, _ := json.MarshalIndent(p.data, "", "\t")
		_ = WriteInFile(p.file, data)
		return pet, nil
	}
	return entity.Pet{}, fmt.Errorf("id not found")
}

func (p *PetStorage) FindByStatus(status string) ([]*entity.Pet, error) {

	switch status {
	case "available":
		break
	case "pending":
		break
	case "sold":
		break
	default:
		return []*entity.Pet{}, fmt.Errorf("status is not evailable")
	}

	resDataStatus := make([]*entity.Pet, 0, 13)
	for i := range p.data {
		if p.data[i].Status == status {
			resDataStatus = append(resDataStatus, p.data[i])

		}
	}
	if len(resDataStatus) > 0 {
		return resDataStatus, nil
	}

	return []*entity.Pet{}, fmt.Errorf("pet with this status not found")
}

func (p *PetStorage) Delete(petID int) error {
	p.Lock()
	defer p.Unlock()
	if v, ok := p.primaryKeyIDx[int64(petID)]; ok {
		delete(p.primaryKeyIDx, v.ID)
		for i := range p.data {
			if p.data[i] == v {
				p.data = append(p.data[:i], p.data[i+1:]...)
				break
			}
		}
		data, _ := json.MarshalIndent(p.data, "", "\t")
		_ = WriteInFile(p.file, data)
		return nil
	}
	return fmt.Errorf("index not found")
}

func (p *PetStorage) GetID(petID int) (entity.Pet, error) {
	if v, ok := p.primaryKeyIDx[int64(petID)]; ok {
		return *v, nil
	}
	return entity.Pet{}, fmt.Errorf("not found")
}

func (p *PetStorage) UpdatePOST(petID int, name, status string) error {
	p.Lock()
	defer p.Unlock()
	if _, ok := p.primaryKeyIDx[int64(petID)]; ok {
		p.primaryKeyIDx[int64(petID)].Status = status
		p.primaryKeyIDx[int64(petID)].Name = name
		for i := range p.data {
			if p.data[i].ID == int64(petID) {
				p.data[i].Status = status
				p.data[i].Name = name
			}
		}
		data, _ := json.MarshalIndent(p.data, "", "\t")
		_ = WriteInFile(p.file, data)

		return nil
	}
	return fmt.Errorf("not found")
}
