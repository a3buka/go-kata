package handler

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"

	"gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/internal/entity"
)

func (h *Handler) createUser(w http.ResponseWriter, r *http.Request) {
	var user entity.User
	err := json.NewDecoder(r.Body).Decode(&user)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	user, err = h.service.UserServiecer.Create(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (h *Handler) createWithArray(w http.ResponseWriter, r *http.Request) {
	var user []entity.User
	err := json.NewDecoder(r.Body).Decode(&user)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	user = h.service.UserServiecer.CreateWithArray(user)

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

//func (h *Handler) createWithList(w http.ResponseWriter, r *http.Request) {
//	var user []entity.User
//	err := json.NewDecoder(r.Body).Decode(&user)
//	defer r.Body.Close()
//	if err != nil {
//		http.Error(w, err.Error(), http.StatusBadRequest)
//		return
//	}
//	user = h.service.UserServiecer.CreateWithList(user)
//	w.Header().Set("Content-Type", "application/json;charset=utf-8")
//	err = json.NewEncoder(w).Encode(user)
//	if err != nil {
//		http.Error(w, err.Error(), http.StatusInternalServerError)
//		return
//	}
//}

func (h *Handler) userGetWithUsername(w http.ResponseWriter, r *http.Request) {
	var (
		user     entity.User
		err      error
		username string
	)
	username = chi.URLParam(r, "username")
	user, err = h.service.UserServiecer.GetUsername(username)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (h *Handler) userUpdateWithUsername(w http.ResponseWriter, r *http.Request) {
	var (
		user     entity.User
		err      error
		username string
	)
	username = chi.URLParam(r, "username")
	err = json.NewDecoder(r.Body).Decode(&user)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	user, err = h.service.UserServiecer.Update(username, user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}
func (h *Handler) userDeleteWithUsername(w http.ResponseWriter, r *http.Request) {
	var (
		user     entity.User
		err      error
		username string
	)
	username = chi.URLParam(r, "username")
	err = h.service.UserServiecer.Delete(username)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
