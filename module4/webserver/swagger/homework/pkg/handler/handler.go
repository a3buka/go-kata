package handler

import (
	"net/http"
	"text/template"
	"time"

	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/chi/v5"
	"gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/serviece"
)

const (
	swaggerTemplate = `<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-standalone-preset.js"></script> -->
    <script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui-bundle.js"></script> -->
    <link rel="stylesheet" href="//unpkg.com/swagger-ui-dist@3/swagger-ui.css" />
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.22.1/swagger-ui.css" /> -->
	<style>
		body {
			margin: 0;
		}
	</style>
    <title>Swagger</title>
</head>
<body>
    <div id="swagger-ui"></div>
    <script>
        window.onload = function() {
          SwaggerUIBundle({
            url: "/public/swagger.json?{{.Time}}",
            dom_id: '#swagger-ui',
            presets: [
              SwaggerUIBundle.presets.apis,
              SwaggerUIStandalonePreset
            ],
            layout: "StandaloneLayout"
          })
        }
    </script>
</body>
</html>
`
)

func swaggerUI(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	tmpl, err := template.New("swagger").Parse(swaggerTemplate)
	if err != nil {
		return
	}
	err = tmpl.Execute(w, struct {
		Time int64
	}{
		Time: time.Now().Unix(),
	})
	if err != nil {
		return
	}
}

type Handler struct {
	service *serviece.Service
}

func NewHandler(service *serviece.Service) *Handler {
	return &Handler{service: service}
}

func (h *Handler) InitRoutes() *chi.Mux {
	router := chi.NewRouter()
	router.Use(middleware.Logger)

	router.Route("/pet", func(pet chi.Router) {

		pet.Post("/{petID}/uploadImage", h.UploadImagePet)
		pet.Post("/", h.CreatePet)
		pet.Put("/", h.UpdatePetPUT)
		pet.Get("/findGetByStatus", h.GetsByStatus)
		pet.Get("/{petID}", h.GetPet)
		pet.Delete("/{petID}", h.DeletePet)
		pet.Post("/{petID}", h.PetUpdate)
	})

	router.Route("/user", func(user chi.Router) {

		user.Post("/", h.createUser)
		user.Post("/createWithArray", h.createWithArray)
		//user.Post("/createWithList", h.createWithList)
		user.Get("/{username}", h.userGetWithUsername)
		user.Put("/{username}", h.userUpdateWithUsername)
		user.Delete("/{username}", h.userDeleteWithUsername)
	})

	router.Route("/store", func(store chi.Router) {
		store.Post("/order", h.createOrder)
		store.Get("/inventory", h.getAllOrders)
		store.Get("/order/{id}", h.getOrder)
		store.Delete("/{id}", h.deleteOrder)
	})

	router.Get("/swagger", swaggerUI)
	router.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))).ServeHTTP(w, r)
	})

	return router
}
