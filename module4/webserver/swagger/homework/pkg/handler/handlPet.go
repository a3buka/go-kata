package handler

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/go-chi/chi/v5"

	"gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/internal/entity"
)

var (
	IMAGE_TYPES = map[string]interface{}{
		"image/jpeg": nil,
		"image/png":  nil,
	}
)

func (h *Handler) UploadImagePet(w http.ResponseWriter, r *http.Request) {

	petIDRaw := chi.URLParam(r, "petID") // получаем petID из chi router

	petID, err := strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                      // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	r.Body = http.MaxBytesReader(w, r.Body, 5<<20) // максимально допустимый размер файла 5 МБ
	file, fileHeader, err := r.FormFile("file")
	defer file.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	buffer := make([]byte, fileHeader.Size)
	file.Read(buffer)
	fileType := http.DetectContentType(buffer)
	if _, ex := IMAGE_TYPES[fileType]; !ex {
		http.Error(w, "file type is not supported", http.StatusBadRequest)
	}
	err = h.service.PetServicer.UploadImage(petID, fileHeader.Filename)
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	f, err := os.OpenFile("./img/"+strconv.Itoa(petID)+fileHeader.Filename, os.O_WRONLY|os.O_CREATE, 0644)
	if err != nil {
		fmt.Errorf("error file loading")
	}
	defer f.Close()
	io.Copy(f, file)

}

func (h *Handler) CreatePet(w http.ResponseWriter, r *http.Request) {
	var pet entity.Pet
	err := json.NewDecoder(r.Body).Decode(&pet)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	pet = h.service.PetServicer.Create(pet)
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(pet)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (h *Handler) UpdatePetPUT(w http.ResponseWriter, r *http.Request) {
	var pet entity.Pet
	err := json.NewDecoder(r.Body).Decode(&pet)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
	pet, err = h.service.PetServicer.Update(pet)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(pet)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (h *Handler) GetsByStatus(w http.ResponseWriter, r *http.Request) {
	var (
		pet    []*entity.Pet
		err    error
		status string
	)
	values := r.URL.Query()

	for statusQuery, item := range values {
		if statusQuery == "status" {
			status = item[0]
			break
		}
	}
	pet, err = h.service.PetServicer.FindByStatus(strings.TrimSpace(status))
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(pet)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (h *Handler) GetPet(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		pet      entity.Pet
		err      error
		petIDRaw string
		petID    int
	)

	petIDRaw = chi.URLParam(r, "petID") // получаем petID из chi router

	petID, err = strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                     // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	pet, err = h.service.PetServicer.GetID(petID) // пытаемся получить Pet по id
	if err != nil {                               // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(pet)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

}

//func (h *Handler) GetsPet(w http.ResponseWriter, r *http.Request) {
//
//}

func (h *Handler) DeletePet(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing

		err      error
		petIDRaw string
		petID    int
	)

	petIDRaw = chi.URLParam(r, "petID") // получаем petID из chi router

	petID, err = strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                     // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
	err = h.service.PetServicer.Delete(petID)
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (h *Handler) PetUpdate(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		err      error
		petIDRaw string
		petID    int
		name     string
		status   string
	)

	petIDRaw = chi.URLParam(r, "petID") // получаем petID из chi router

	petID, err = strconv.Atoi(petIDRaw) // конвертируем в int
	if err != nil {                     // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
	err = r.ParseForm()
	if err != nil { // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
	name = r.FormValue("name")
	status = r.FormValue("status")
	err = h.service.PetServicer.UpdatePOST(petID, name, status)
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}
