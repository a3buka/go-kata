package handler

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"

	"gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/internal/entity"
)

func (h *Handler) createOrder(w http.ResponseWriter, r *http.Request) {
	var order entity.Store
	err := json.NewDecoder(r.Body).Decode(&order)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}

	order = h.service.StoreServicer.Create(order)
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(order)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (h *Handler) getOrder(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowing
		order    entity.Store
		err      error
		orderRaw string
		orderID  int
	)

	orderRaw = chi.URLParam(r, "id")
	orderID, err = strconv.Atoi(orderRaw) // конвертируем в int
	if err != nil {                       // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	order, err = h.service.StoreServicer.GetID(orderID) // пытаемся получить Pet по id
	if err != nil {                                     // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(order)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (h *Handler) deleteOrder(w http.ResponseWriter, r *http.Request) {
	var ( // заранее аллоцируем все необходимые переменные во избежание shadowin
		err      error
		orderRaw string
		orderID  int
	)

	orderRaw = chi.URLParam(r, "id")
	orderID, err = strconv.Atoi(orderRaw) // конвертируем в int
	if err != nil {                       // в случае ошибки отправляем код 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
	err = h.service.StoreServicer.Delete(orderID)
	if err != nil { // в случае ошибки отправляем Not Found код 404
		http.Error(w, err.Error(), http.StatusNotFound)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}

func (h *Handler) getAllOrders(w http.ResponseWriter, r *http.Request) {
	var (
		orders map[string]int
		err    error
	)

	orders = h.service.StoreServicer.GetAll()
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(orders)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
}
