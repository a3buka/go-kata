package main

import "gitlab.com/a3buka/go-kata/module4/webserver/swagger/homework/cmd/app"

func main() {
	app.Run()
}
