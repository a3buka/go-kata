package main

import (
	"bufio"
	"fmt"
	"net"
	"strings"
)

func handlerConnection(conn net.Conn) {
	defer conn.Close()
	reader := bufio.NewReader(conn)

	//Прочитать запрос
	request, _ := reader.ReadString('\n')
	fmt.Println(request)

	//Разделить запрос
	parts := strings.Split(request, " ")
	method := parts[0]
	path := parts[1]

	//Write the response - Ответ на запрос
	if method == "GET" && path == "/" {
		fmt.Fprintf(conn, "HTTP/1.1 200 OK\r\n\nHello, World!")
	} else {
		fmt.Fprintf(conn, "HTTP/1.1 400 Not found\r\n\r\n")
	}

}

func main() {
	listner, _ := net.Listen("tcp", ":8080")
	defer listner.Close()
	for {
		conn, _ := listner.Accept()
		go handlerConnection(conn)
	}
}
