package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"strings"
)

func handleConnection(conn net.Conn) {
	defer conn.Close()
	reader := bufio.NewReader(conn)

	//прочитаем запрос
	request, _ := reader.ReadString('\n')
	fmt.Println(request)

	//парсинг запроса
	parts := strings.Split(request, " ")
	if len(parts) < 2 {
		return
	}
	method := parts[0]
	path := parts[1]

	//написание на сайте
	switch {
	case method == "GET" && path == "/":
		m := "HTTP/1.1 200 OK\n"
		m = m + "Server: nginx/1.19.2\n"
		m = m + "Date: Mon, 19 Oct 2020 13:13:29 GMT\n"
		m = m + "Content-Type: text/html\n"
		m = m + "Content-Length: 98\n" // указываем размер контента
		m = m + "Last-Modified: Mon, 19 Oct 2020 13:13:13 GMT\n"
		m = m + "Connection: keep-alive\n"
		m = m + "ETag: \"5f8d90e9-62\"\n"
		m = m + "Accept-Ranges: bytes\n"
		m = m + "\n"
		m = m + "<!DOCTYPE html>\n"
		m = m + "<html>\n"
		m = m + "<head>\n"
		m = m + "<title>Webserver</title>\n"
		m = m + "</head>\n"
		m = m + "<body>\n"
		m = m + "hello world\n"
		m = m + "</body>\n"
		m = m + "</html>\n"
		_, _ = conn.Write([]byte(m))
	case method == "POST" && path == "/upload":
		//прочитайте файл чтобы отправить в тело
		file, _ := os.Create("upload_file.txt")
		defer file.Close()
		_, err := io.Copy(file, reader)
		if err != nil {
			fmt.Println(err)
		}

		fmt.Fprintf(conn, "HTTP/1.1 200 OK\r\n\r\nFile upload successfully")
		return
	default:
		fmt.Fprintf(conn, "HTTP/1.1 404 Not Found\r\n\r\n")
	}

}

func main() {
	listner, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal(err)
	}
	defer listner.Close()

	for {
		conn, _ := listner.Accept()
		handleConnection(conn)
	}

	//
}
