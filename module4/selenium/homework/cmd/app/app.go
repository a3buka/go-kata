package app

import (
	"context"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"gitlab.com/a3buka/go-kata/module4/selenium/homework/internal/server"

	handler2 "gitlab.com/a3buka/go-kata/module4/selenium/homework/pkg/handler"

	"github.com/sirupsen/logrus"
	repository2 "gitlab.com/a3buka/go-kata/module4/selenium/homework/pkg/repository"
	service2 "gitlab.com/a3buka/go-kata/module4/selenium/homework/service"
)

func RunSelenium() {
	repository := repository2.NewRepositorySelenium()
	service := service2.NewVacansyService(repository)
	handler := handler2.NewHandler(service)
	srv := server.NewServer(handler.InitRoutes())

	go func() {
		if err := srv.Run(); !errors.Is(err, http.ErrServerClosed) {
			logrus.Errorf("server it's not worked %s", err.Error())
		}
	}()

	quit := make(chan os.Signal, 1)
	signal.Notify(quit, syscall.SIGTERM, syscall.SIGINT)
	<-quit

	logrus.Print("TodoApp Shutting Down")

	if err := srv.Shutdown(context.Background()); err != nil {
		logrus.Errorf("error occured on server shutting down: %s", err.Error())
	}

}
