package selenium

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/tebeka/selenium"
	"gitlab.com/a3buka/go-kata/module4/selenium/homework/internal/models"
)

const (
	maxTries   = 5
	driverPort = 4444
)

func SeleniumPArseFireFox(name string) []models.Vacancy {
	// Прописываем конфигурацию для драйвера
	caps := selenium.Capabilities{
		"browserName": "firefox",
	}

	// Создаем новый драйвер с заданными настройками
	var wd selenium.WebDriver
	var err error
	for i := 0; i < maxTries; i++ {
		// Соединяемся с драйвером
		wd, err = selenium.NewRemote(caps, fmt.Sprintf("http://selenium:%d/wd/hub", driverPort))
		if err == nil {
			break
		}
		log.Printf("Error connecting to remote driver: %v", err)
		time.Sleep(5 * time.Second)
	}

	if wd == nil {
		log.Fatalf("Failed to connect to remote driver after %d attempts", maxTries)
	}
	defer wd.Quit()

	// Сразу обращаемся к странице с поиском вакансии по запросу
	page := 1     // номер страницы
	query := name // запрос
	wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query))

	//Получение элементов со страницы
	elem, err := wd.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		log.Fatalln(err)
	}
	// Сколько вакансий мы имеем
	vacancyCountRow, err := elem.Text() //"Получено 28 вакансий"
	if err != nil {
		log.Fatalln(err)
	}

	vacancyCount, err := strconv.Atoi(strings.Fields(vacancyCountRow)[1]) // Забираем только int выражение

	countOfPage := vacancyCount / 25
	if countOfPage%25 > 0 {
		countOfPage++
	}

	var links []string
	var list []models.Vacancy
	var countVacancy, errUnmarhal, autoIncrementId int

	for i := 1; i <= countOfPage; i++ { // Сначала передвигаемся по страницам
		wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", i, query))

		elems, err := wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
		if err != nil {
			log.Fatalln(err)
		}

		for j := range elems { // Здесь начнинаем передвигаться по элементам
			link, err1 := elems[j].GetAttribute("href")
			log.Printf("Вакансия найдена: %s\n", link)
			//Если ссылка не рабоатет пропускаем
			if err1 != nil {
				continue
			}

			resp, err := http.Get("https://career.habr.com" + link) //обращаемся к конкретной вакансии
			if err != nil {
				log.Println(err)
			}
			//Считываем тело запроса
			var doc *goquery.Document
			doc, err = goquery.NewDocumentFromReader(resp.Body)
			if err != nil && doc != nil {
				log.Println(err)
			}
			//Преобразуем тело запроса в джейсон
			dd := doc.Find("script[type=\"application/ld+json\"]")
			if dd == nil {
				log.Fatalln("habr vacancy nodes not found")
			}

			var vacancy models.Vacancy
			//Преобразуем в байты
			b := bytes.NewBufferString(dd.First().Text())
			//Записываем в джеймон
			err = json.Unmarshal(b.Bytes(), &vacancy)
			if err != nil {
				log.Println(err)
				//Счетчик ошибок
				errUnmarhal++
			}
			vacancy.Id = strconv.Itoa(autoIncrementId)
			autoIncrementId++
			//Удачные вакансии
			list = append(list, vacancy)
			countVacancy++

			links = append(links, link)
		}
		//Для проверки
		fmt.Printf("Количество вакансий: %d\nКоличество ошибок: %d\n", countVacancy, errUnmarhal)
		log.Println("Done parsing")
	}
	return list
}
