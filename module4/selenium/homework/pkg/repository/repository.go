package repository

import "gitlab.com/a3buka/go-kata/module4/selenium/homework/internal/models"

type Selemiumer interface {
	Create(name string) error
	GetById(id int) (models.Vacancy, error)
	GetList() ([]models.Vacancy, error)
	Delete(id int) error
}

type RepositorySelenium struct {
	Selemiumer
}

func NewRepositorySelenium() *RepositorySelenium {
	return &RepositorySelenium{Selemiumer: NewSeleniumSrorage()}
}
