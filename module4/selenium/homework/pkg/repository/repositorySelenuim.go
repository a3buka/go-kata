package repository

import (
	"errors"

	"gitlab.com/a3buka/go-kata/module4/selenium/homework/pkg/selenium"

	"gitlab.com/a3buka/go-kata/module4/selenium/homework/internal/models"
)

type SelenuimStorage struct {
	data map[int]models.Vacancy
}

func NewSeleniumSrorage() *SelenuimStorage {
	return &SelenuimStorage{
		data: make(map[int]models.Vacancy, 13),
	}
}

func (s SelenuimStorage) Create(name string) error {
	list := selenium.SeleniumPArseFireFox(name)
	if len(list) > 0 {
		for i := range list {
			s.data[i] = list[i]
		}
		return nil
	}
	return errors.New("not create")
}

func (s SelenuimStorage) GetById(id int) (models.Vacancy, error) {
	if v, ok := s.data[id]; ok {
		return v, nil
	}
	return models.Vacancy{}, errors.New("id not found")
}

func (s SelenuimStorage) GetList() ([]models.Vacancy, error) {
	var vacancies []models.Vacancy
	for _, idx := range s.data {
		vacancies = append(vacancies, idx)
	}
	if len(vacancies) > 0 {
		return vacancies, nil
	}
	return []models.Vacancy{}, errors.New("not found")
}

func (s SelenuimStorage) Delete(id int) error {
	if _, ok := s.data[id]; ok {
		delete(s.data, id)
		return nil
	}
	return errors.New("not found")
}
