package main

import (
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"

	"github.com/tebeka/selenium"
)

const (
	maxTries   = 5
	driverPort = 4444
)

func main() {
	// Прописываем конфигурацию для драйвера
	caps := selenium.Capabilities{
		"browserName": "firefox",
	}

	// Создаем новый драйвер с заданными настройками
	var wd selenium.WebDriver
	var err error
	for i := 0; i < maxTries; i++ {
		// Соединяемся с драйвером
		wd, err = selenium.NewRemote(caps, fmt.Sprintf("http://localhost:%d/wd/hub", driverPort))
		if err == nil {
			break
		}
		log.Printf("Error connecting to remote driver: %v", err)
		time.Sleep(5 * time.Second)
	}

	if wd == nil {
		log.Fatalf("Failed to connect to remote driver after %d attempts", maxTries)
	}
	defer wd.Quit()

	// Сразу обращаемся к странице с поиском вакансии по запросу
	page := 1       // номер страницы
	query := "ruby" // запрос
	wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query))

	//Получение элементов со страницы
	elem, err := wd.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		log.Fatalln(err)
	}
	// Сколько вакансий мы имеем
	vacancyCountRow, err := elem.Text() //"Получено 28 вакансий"
	if err != nil {
		log.Fatalln(err)
	}

	vacancyCount, err := strconv.Atoi(strings.Fields(vacancyCountRow)[1]) // Забираем только int выражение

	countOfPage := vacancyCount / 25
	if countOfPage%25 > 0 {
		countOfPage++
	}

	//var links []string
	//var list []models.Vacancy
	//var countVacancy, errUnmarhal int

	for i := 1; i <= countOfPage; i++ { // Сначала передвигаемся по страницам
		wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", i, query))

		elems, err := wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
		if err != nil {
			log.Fatalln(err)
		}

		for j := range elems { // Здесь начнинаем передвигаться по элементам
			link, err1 := elems[j].GetAttribute("href")
			log.Printf("Вакансия найдена: %s\n", link)
			//Если ссылка не рабоатет пропускаем
			if err1 != nil {
				continue
			}

			resp, err := http.Get("https://career.habr.com" + link) //обращаемся к конкретной вакансии
			if err != nil {
				log.Println(err)
			}
			//Считываем тело запроса
			var doc *goquery.Document
			doc, err = goquery.NewDocumentFromReader(resp.Body)
			if err != nil && doc != nil {
				log.Println(err)
			}
			//Преобразуем тело запроса в джейсон
			dd := doc.Find("script[type=\"application/ld+json\"]")
			if dd == nil {
				log.Fatalln("habr vacancy nodes not found")
			}
			ss := dd.First().Text()
			fmt.Println(ss)
		}

	}

	///
	//elems, err := wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
	//if err != nil {
	//	panic(err)
	//}
	//var links []string
	//for i := range elems {
	//	var link string
	//	link, err = elems[i].GetAttribute("href")
	//	if err != nil {
	//		continue
	//	}
	//	links = append(links, link)
	//}
	//

	// Ждем 60 секунд, чтобы успеть посмотреть результат
	time.Sleep(60 * time.Second)

}

func FindCount(wd selenium.WebDriver) (int, error) {
	elem, err := wd.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		return 0, err
	}

	vacancyCountRaw, err := elem.Text()
	if err != nil {
		return 0, err
	}

	vacancyCount, err := strconv.Atoi(vacancyCountRaw)
	if err != nil {
		return 1, err
	}

	return vacancyCount, nil
}

func lintVacancy(wd selenium.WebDriver) ([]string, error) {
	elems, err := wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
	if err != nil {
		return nil, err
	}
	var links []string
	for i := range elems {
		var link string
		link, err = elems[i].GetAttribute("href")
		if err != nil {
			continue
		}
		links = append(links, link)
	}
	return links, nil
}
