package algo

import (
	"math/rand"
	"reflect"
	"sort"
	"testing"
	"time"
)

func randomData(n, max int) func() []int {

	var data []int
	return func() []int {
		if data != nil {
			return data
		}
		rand.Seed(time.Now().UnixNano())
		for i := 0; i < n; i++ {
			data = append(data, rand.Intn(max))
		}

		return data
	}
}

func Test_mergeSort(t *testing.T) {
	data := randomData(100, 1000)
	sorted := append([]int{}, data()...)
	sort.Ints(sorted)
	type args struct {
		data []int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "Sort reversed slice",
			args: args{
				data: []int{55, 34, 21, 13, 8, 5, 3, 2, 1, 1},
			},
			want: []int{1, 1, 2, 3, 5, 8, 13, 21, 34, 55},
		},
		{
			name: "random 1000 numbers",
			args: args{
				data: data(),
			},
			want: sorted,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := mergeSort(tt.args.data); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("merge() = %v, want %v", got, tt.want)
			}
		})
	}
}
