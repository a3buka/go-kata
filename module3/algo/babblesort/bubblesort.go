package babblesort

func BubbleSort(data []int) []int {
	swaped := true
	i := 1
	for swaped {
		swaped = false
		for j := 0; j < len(data)-i; j++ {

			if data[j] > data[j+1] {
				data[j], data[j+1] = data[j+1], data[j]
				swaped = true
			}
		}
		i++
	}
	return data
}
