package main

import (
	repository "gitlab.com/a3buka/go-kata/module3/clean_architecture/cli/repo"
	"gitlab.com/a3buka/go-kata/module3/clean_architecture/cli/service"
	"gitlab.com/a3buka/go-kata/module3/clean_architecture/cli/useCli"
)

func main() {

	r := repository.New()
	s := service.New(r)
	c := useCli.NewCli(s)

	c.Run()
}
