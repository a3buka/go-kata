package service

import (
	"gitlab.com/a3buka/go-kata/module3/clean_architecture/cli/model"
)

// ToDoRepository is a repository interface for tasks
type ToDoRepository interface {
	GetTasks() ([]model.Todo, error)
	GetTask(id int) (model.Todo, error)
	CreateTask(task model.Todo) error
	UpdateTask(task model.Todo) error
	RemoveTask(id int) error
}

type ToDoService struct {
	repository ToDoRepository
}

func New(repo ToDoRepository) *ToDoService {
	return &ToDoService{
		repository: repo,
	}
}

// GetTasks return all tasks from repository
func (s *ToDoService) GetTasks() ([]model.Todo, error) {
	tasks, err := s.repository.GetTasks()
	if err != nil {
		return nil, err
	}

	return tasks, nil
}

// GetTask return task with id from repository
func (s *ToDoService) GetTask(id int) (model.Todo, error) {
	task, err := s.repository.GetTask(id)
	if err != nil {
		return task, err
	}

	return task, nil
}

// CreateTask task in repository
func (s *ToDoService) CreateTask(title, description string) error {
	t := model.Todo{
		Title:       title,
		Description: description,
		Status:      "started",
	}

	err := s.repository.CreateTask(t)
	if err != nil {
		return err
	}

	return nil
}

// CompleteTask : task.status = "done"
func (s *ToDoService) CompleteTask(id int) error {
	t, err := s.GetTask(id)
	if err != nil {
		return err
	}

	t.Status = "done"

	err = s.repository.UpdateTask(t)
	if err != nil {
		return err
	}

	return nil
}

// UpdateTask task from repository
func (s *ToDoService) UpdateTask(t model.Todo) error {
	err := s.repository.UpdateTask(t)
	if err != nil {
		return err
	}

	return nil
}

// RemoveTask task from repository
func (s *ToDoService) RemoveTask(id int) error {
	err := s.repository.RemoveTask(id)
	if err != nil {
		return err
	}

	return nil
}
