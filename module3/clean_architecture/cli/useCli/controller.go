package useCli

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"gitlab.com/a3buka/go-kata/module3/clean_architecture/cli/model"
)

type ToDoService interface {
	GetTasks() ([]model.Todo, error)
	GetTask(id int) (model.Todo, error)
	CreateTask(title, description string) error
	UpdateTask(task model.Todo) error
	RemoveTask(id int) error
}

type ToDoCli struct {
	controller ToDoService
}

func NewCli(s ToDoService) *ToDoCli {
	return &ToDoCli{controller: s}
}

func (c *ToDoCli) Run() {
	begin := "--------MENU--------\n" +
		"Выберите подходящий вариант из списка ниже:\n" +
		"1. Показать список задач\n" +
		"2. Добавить задачу\n" +
		"3. Удалить задачу\n" +
		"4. Редактировать задачу по ID\n" +
		"5. Завершить задачу\n" +
		"6. Выход"

	var inputNumber int
	for {
		fmt.Println(begin)
		_, _ = fmt.Scan(&inputNumber)
		switch inputNumber {
		case 1:
			c.ShowAllTask()
		case 2:
			c.AddTask()
		case 3:
			c.RemoveTask()
		case 4:
			c.ChangeTaskWithId()
		case 5:
			c.ChangeTaskStatus()
		case 6:
			return
		default:
			fmt.Println("Необходимо выбрать цифру от 1 до 6")
		}
	}
}

func (c *ToDoCli) ShowAllTask() {
	tasks, err := c.controller.GetTasks()
	if err != nil {
		fmt.Println(err)
	}
	if tasks == nil {
		fmt.Println("Список задач пуст.")
	} else {
		for i := range tasks {
			fmt.Printf("ID: %v | tittle: %v | desscription: %v | status: %v\n", tasks[i].ID, tasks[i].Title, tasks[i].Description, tasks[i].Status) //необходимо выводить по тодо структуре
		}
	}

}

func (c *ToDoCli) AddTask() {
	//var tittle, description string

	fmt.Print("Введите заголовок задачи: ")
	tittle, err := bufio.NewReader(os.Stdin).ReadString('\n') //разделитель при нажатии на enter
	if err != nil {
		fmt.Println("Недопустимое описание заголовка задачи")
		return
	}
	tittle = strings.TrimSpace(tittle) //удаления ентера в конце чтобы в джейсон файле не было переноса но новую строку

	fmt.Print("Напишите описание введенной задачи: ")
	description, err := bufio.NewReader(os.Stdin).ReadString('\n')
	if err != nil {
		fmt.Println("Недопустимое описание")
		return
	}
	description = strings.TrimSpace(description)
	err = c.controller.CreateTask(tittle, description)
	if err != nil {
		fmt.Println("Ошибка в добавлении задачи")
	} else {
		fmt.Println("Задача успешно добавлена")
	}
}

func (c *ToDoCli) RemoveTask() {
	fmt.Print("Необходимо ввести цифру задачи которую вы хотите удалить: ")
	var number int
	_, err := fmt.Fscan(os.Stdin, &number)
	if err != nil {
		fmt.Println(err)
		return
	}
	err = c.controller.RemoveTask(number)
	if err != nil {
		fmt.Println(err)
		return
	} else {
		fmt.Println("Задача успешно удалена")
	}

}

func (c *ToDoCli) ChangeTaskWithId() {
	fmt.Print("Введите id задачи для которой необходимо изменить задание и описание: ")
	var number int
	_, err := fmt.Fscan(os.Stdin, &number)
	if err != nil {
		fmt.Println("Неверно указан id задачи")

	}
	todo, err := c.controller.GetTask(number)
	if err != nil {
		fmt.Println(err)

	}

	fmt.Print("Введите заголовок задачи: ")
	tittle, err := bufio.NewReader(os.Stdin).ReadString('\n') //разделитель при нажатии на enter
	if err != nil {
		fmt.Println("Недопустимое описание заголовка задачи")
		return
	}
	tittle = strings.TrimSpace(tittle) //удаления ентера в конце чтобы в джейсон файле не было переноса но новую строку

	fmt.Print("Напишите описание введенной задачи: ")
	description, err := bufio.NewReader(os.Stdin).ReadString('\n')
	if err != nil {
		fmt.Println("Недопустимое описание")
		return
	}
	description = strings.TrimSpace(description)

	todo.Title = tittle
	todo.Description = description

	err = c.controller.UpdateTask(todo)
	if err != nil {
		fmt.Println("Обновление не выполненно")
	}

	fmt.Println("Success")

}

func (c *ToDoCli) ChangeTaskStatus() {
	fmt.Print("Введите id задачи для которой необходимо изменить статус: ")
	var number int
	_, err := fmt.Fscan(os.Stdin, &number)
	if err != nil {
		fmt.Println(err)
	}
	todo, _ := c.controller.GetTask(number)
	if todo.Status == "finished" {
		fmt.Println("статус этой задачи уже изменен")
		return
	}

	todo.Status = "finished"
	err = c.controller.UpdateTask(todo)
	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Println("статус изменен")
	}

}
