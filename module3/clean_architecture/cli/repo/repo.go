package repository

import (
	"encoding/json"
	"errors"
	"os"

	"gitlab.com/a3buka/go-kata/module3/clean_architecture/cli/model"
)

const (
	ErrTaskNotFound = "такой задачи нет в списке"
)

// FileTaskRepository is a file-based implementation of TaskRepository
type FileTaskRepository struct {
	filepath string
}

func New() *FileTaskRepository {
	return &FileTaskRepository{filepath: "/Users/admin/go/src/gitlab.com/a3buka/go-kata/module3/clean_architecture/cli/cli.json\n"}
}

// GetTasks returns all tasks from the repository
func (repo *FileTaskRepository) GetTasks() ([]model.Todo, error) {
	var tasks []model.Todo
	// File not exist
	if _, err := os.Stat(repo.filepath); err != nil {
		return tasks, nil
	}
	data, err := os.ReadFile(repo.filepath)
	if err != nil {
		return nil, err
	}
	if err := json.Unmarshal(data, &tasks); err != nil {
		return nil, err
	}
	return tasks, nil
}

// GetTask return task by id
func (repo *FileTaskRepository) GetTask(id int) (model.Todo, error) {
	tasks, err := repo.GetTasks()
	if err != nil {
		return model.Todo{}, err
	}
	for _, v := range tasks {
		if v.ID == id {
			return v, nil
		}
	}
	return model.Todo{}, errors.New(ErrTaskNotFound)
}

// CreateTask adds a new task to the repository
func (repo *FileTaskRepository) CreateTask(task model.Todo) error {
	tasks, err := repo.GetTasks()
	if err != nil {
		return err
	}
	if len(tasks) > 0 {
		task.ID = tasks[len(tasks)-1].ID + 1
	} else {
		task.ID = 0
	}
	tasks = append(tasks, task)
	if err := repo.saveTasks(tasks); err != nil {
		return err
	}
	return nil
}

// UpdateTask updates an existing task in the repository
func (repo *FileTaskRepository) UpdateTask(task model.Todo) error {
	tasks, err := repo.GetTasks()
	if err != nil {
		return err
	}
	if _, err = repo.GetTask(task.ID); err != nil {
		return errors.New(ErrTaskNotFound)
	}
	for i, t := range tasks {
		if t.ID == task.ID {
			tasks[i] = task
			break
		}
	}
	if err := repo.saveTasks(tasks); err != nil {
		return err
	}
	return nil
}

// RemoveTask adds a new task to the repository
func (repo *FileTaskRepository) RemoveTask(id int) error {
	tasks, err := repo.GetTasks()
	if err != nil {
		return err
	}
	if _, err = repo.GetTask(id); err != nil {
		return errors.New(ErrTaskNotFound)
	}
	for i, t := range tasks {
		if t.ID == id {
			tasks = append(tasks[:i], tasks[i+1:]...)
		}
	}
	if err := repo.saveTasks(tasks); err != nil {
		return err
	}
	return nil
}

// saveTasks write data to json file
func (repo *FileTaskRepository) saveTasks(tasks []model.Todo) error {
	var data []byte
	var err error
	if data, err = json.MarshalIndent(tasks, " ", "\t"); err != nil {
		return err
	}
	if err = os.WriteFile(repo.filepath, data, 0644); err != nil {
		return err
	}
	return nil
}
