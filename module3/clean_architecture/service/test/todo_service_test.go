package test

import (
	"encoding/json"
	"log"
	"os"
	"testing"

	"gitlab.com/a3buka/go-kata/module3/clean_architecture/service/service"

	"gitlab.com/a3buka/go-kata/module3/clean_architecture/service/model"
	"gitlab.com/a3buka/go-kata/module3/clean_architecture/service/repo"
)

func initTodo() *os.File {
	initialBase := []model.Todo{
		{ID: 1, Title: "Test 1", Description: "1", Status: "process"},
		{ID: 2, Title: "Test 2", Description: "2", Status: "process"},
		{ID: 3, Title: "Test 3", Description: "3", Status: "process"},
	}
	tempFile, err := os.CreateTemp("", "test.temp.json")
	if err != nil {
		log.Fatalln("file it's not create")
	}
	text, err := json.Marshal(initialBase)
	if err != nil {
		log.Fatal("marshal doesn't work")
	}
	_, err = tempFile.Write(text)
	if err != nil {
		log.Fatal("write doesn't work")
	}
	return tempFile
}
func TestTodoService_ListTodos(t *testing.T) {
	file := initTodo()
	defer file.Close()
	pathFile := repo.NewFileTaskRepository(file.Name())

	tests := []struct {
		name   string
		fields service.TodoRepository
		want   []model.Todo
	}{
		{
			name:   "test with all parameters",
			fields: pathFile,
			want: []model.Todo{
				{ID: 1, Title: "Test 1", Description: "1", Status: "process"},
				{ID: 2, Title: "Test 2", Description: "2", Status: "process"},
				{ID: 3, Title: "Test 3", Description: "3", Status: "process"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewTodoService(tt.fields)
			got, _ := s.ListTodos()
			for i := range tt.want {
				if got[i] != tt.want[i] {
					t.Errorf("ListTodos() = %v, want = %v", got[i], tt.want[i])
				}
			}

		})
	}
}

func TestTodoService_CreateTodo(t *testing.T) {
	file := initTodo()
	defer file.Close()
	pathFile := repo.NewFileTaskRepository(file.Name())

	tests := []struct {
		name   string
		fields string
		want   []model.Todo
	}{
		{
			name:   "test with all parameters",
			fields: "Test 4",
			want: []model.Todo{
				{ID: 1, Title: "Test 1", Description: "1", Status: "process"},
				{ID: 2, Title: "Test 2", Description: "2", Status: "process"},
				{ID: 3, Title: "Test 3", Description: "3", Status: "process"},
				{ID: 4, Title: "Test 4"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewTodoService(pathFile)
			_ = s.CreateTodo(tt.fields)
			got, _ := s.ListTodos()
			for i := range tt.want {
				if got[i] != tt.want[i] {
					t.Errorf("ListTodos() = %v, want = %v", got[i], tt.want[i])
				}
			}
		})
	}
}

func TestTodoService_CompleteTodo(t *testing.T) {
	file := initTodo()
	defer file.Close()
	pathFile := repo.NewFileTaskRepository(file.Name())
	tests := []struct {
		name   string
		fields model.Todo
		want   []model.Todo
	}{
		{
			name:   "test with all parameters",
			fields: model.Todo{ID: 3, Title: "Test 3", Description: "3", Status: "process"},
			want: []model.Todo{
				{ID: 1, Title: "Test 1", Description: "1", Status: "process"},
				{ID: 2, Title: "Test 2", Description: "2", Status: "process"},
				{ID: 3, Title: "Test 3", Description: "3", Status: "Complete"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewTodoService(pathFile)
			_ = s.CompleteTodo(tt.fields)
			got, _ := s.ListTodos()
			for i := range tt.want {
				if got[i] != tt.want[i] {
					t.Errorf("ListTodos() = %v, want = %v", got[i], tt.want[i])
				}
			}
		})
	}
}

func TestTodoService_RemoveTodo(t *testing.T) {
	file := initTodo()
	defer file.Close()
	pathFile := repo.NewFileTaskRepository(file.Name())

	tests := []struct {
		name   string
		fields model.Todo
		want   []model.Todo
	}{
		{
			name:   "test with all parameters",
			fields: model.Todo{ID: 2},
			want: []model.Todo{
				{ID: 1, Title: "Test 1", Description: "1", Status: "process"},
				{ID: 3, Title: "Test 3", Description: "3", Status: "process"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := service.NewTodoService(pathFile)
			_ = s.RemoveTodo(tt.fields)
			got, _ := s.ListTodos()
			for i := range tt.want {
				if got[i] != tt.want[i] {
					t.Errorf("ListTodos() = %v, want = %v", got[i], tt.want[i])
				}
			}
		})
	}
}
