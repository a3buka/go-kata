package service

import (
	"errors"

	"gitlab.com/a3buka/go-kata/module3/clean_architecture/service/model"
	"gitlab.com/a3buka/go-kata/module3/clean_architecture/service/repo"
)

type TodoService interface {
	ListTodos() ([]model.Todo, error)
	CreateTodo(title string) error
	CompleteTodo(todo model.Todo) error
	RemoveTodo(todo model.Todo) error
}

type TodoRepository interface {
	GetTasks() ([]repo.Task, error)
	GetTask(id int) (repo.Task, error)
	CreateTask(task repo.Task) (repo.Task, error)
	UpdateTask(task repo.Task) (repo.Task, error)
	DeleteTask(id int) error
}

type todoService struct {
	repository TodoRepository
}

func NewTodoService(r TodoRepository) *todoService {
	return &todoService{repository: r}
}

func (s *todoService) ListTodos() ([]model.Todo, error) {
	tasks, err := s.repository.GetTasks()
	if err != nil {
		return nil, errors.New("get tasks rep doesn't work")
	}
	modTodo := make([]model.Todo, len(tasks))
	if len(tasks) == 1 {
		modTodo[0].ID, modTodo[0].Status, modTodo[0].Description, modTodo[0].Title = tasks[0].ID, tasks[0].Status, tasks[0].Description, tasks[0].Title
	} else {
		for i, item := range tasks {
			modTodo[i].ID, modTodo[i].Status, modTodo[i].Description, modTodo[i].Title = item.ID, item.Status, item.Description, item.Title
		}
	}
	return modTodo, nil
}

func (s *todoService) CreateTodo(title string) error {
	task := repo.Task{Title: title}
	_, err := s.repository.CreateTask(task)
	if err != nil {
		return err
	}
	return nil
}

func (s *todoService) CompleteTodo(todo model.Todo) error {
	task := repo.Task{ID: todo.ID, Description: todo.Description, Title: todo.Title, Status: "Complete"}
	_, err := s.repository.UpdateTask(task)
	if err != nil {
		return err
	}
	return nil
}

func (s *todoService) RemoveTodo(todo model.Todo) error {
	err := s.repository.DeleteTask(todo.ID)
	if err != nil {
		return err
	}
	return nil
}
