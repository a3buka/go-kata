package repo

import (
	"encoding/json"
	"errors"
	"io"
	"os"
)

// Repository layer

// Task represents a to-do task
type Task struct {
	ID          int    `json:"id"`
	Title       string `json:"title"`
	Description string `json:"description"`
	Status      string `json:"status"`
}

// TaskRepository is a repository interface for tasks
type TaskRepository interface {
	GetTasks() ([]Task, error)
	GetTask(id int) (Task, error)
	CreateTask(task Task) (Task, error)
	UpdateTask(task Task) (Task, error)
	DeleteTask(id int) error
	SaveTask(task []Task) error
}

// FileTaskRepository is a file-based implementation of TaskRepository
type FileTaskRepository struct {
	FilePath string
}

func (repo *FileTaskRepository) DeleteTask(id int) error {
	open, err := os.OpenFile(repo.FilePath, os.O_RDWR, 0644)
	if err != nil {
		return err
	}
	defer open.Close()
	tasks, err := repo.GetTasks()
	if err != nil {
		return err
	}
	for i := range tasks {
		if tasks[i].ID == id {
			tasks = append(tasks[:i], tasks[i+1:]...)
			break
		}
		if i == len(tasks)-1 {
			return errors.New("task not find")
		}
	}
	j, err := json.Marshal(tasks)
	if err != nil {
		return err
	}
	_ = open.Truncate(0)
	_, err = open.Seek(0, 0)
	if err != nil {
		return err
	}
	_, err = open.Write(j)
	if err != nil {
		return err
	}
	return nil

}

func NewFileTaskRepository(file string) *FileTaskRepository {
	return &FileTaskRepository{FilePath: file}
}

// GetTasks returns all tasks from the repository
func (repo *FileTaskRepository) GetTasks() ([]Task, error) {
	var tasks []Task

	file, err := os.Open(repo.FilePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	content, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	if err := json.Unmarshal(content, &tasks); err != nil {
		return nil, err
	}

	return tasks, nil
}

// GetTask returns a single task by its ID
func (repo *FileTaskRepository) GetTask(id int) (Task, error) {
	var task Task

	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}

	for _, t := range tasks {
		if t.ID == id {
			return t, nil
		}
	}

	return task, nil
}

// CreateTask adds a new task to the repository
func (repo *FileTaskRepository) CreateTask(task Task) (Task, error) {
	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}

	task.ID = len(tasks) + 1
	tasks = append(tasks, task)

	if err := repo.SaveTask(tasks); err != nil {
		return task, err
	}

	return task, nil
}

// UpdateTask updates an existing task in the repository
func (repo *FileTaskRepository) UpdateTask(task Task) (Task, error) {
	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}

	for i, t := range tasks {
		if t.ID == task.ID {
			tasks[i] = task
			break
		}
	}

	if err := repo.SaveTask(tasks); err != nil {
		return task, err
	}

	return task, nil
}

func (repo *FileTaskRepository) SaveTask(task []Task) error {
	open, err := os.OpenFile(repo.FilePath, os.O_WRONLY|os.O_TRUNC, 0644)
	if err != nil {
		return err
	}
	defer open.Close()
	text, err := json.Marshal(task)
	if err != nil {
		return err
	}
	_, err = open.Write(text)
	if err != nil {
		return err
	}
	return nil
}

//func (repo *FileTaskRepository) Delete(id int) error {
//	open, err := os.OpenFile(repo.FilePath, os.O_RDWR, 0644)
//	if err != nil {
//		return err
//	}
//	defer open.Close()
//	tasks, err := repo.GetTasks()
//	if err != nil {
//		return err
//	}
//	for i := range tasks {
//		if tasks[i].ID == id {
//			tasks = append(tasks[:i], tasks[i+1:]...)
//			break
//		}
//		if i == len(tasks)-1 {
//			return errors.New("task not find")
//		}
//	}
//	j, err := json.Marshal(tasks)
//	if err != nil {
//		return err
//	}
//	_ = open.Truncate(0)
//	_, err = open.Seek(0, 0)
//	if err != nil {
//		return err
//	}
//	_, err = open.Write(j)
//	if err != nil {
//		return err
//	}
//	return nil
//}
