package repository

import (
	"os"
	"reflect"
	"testing"
)

//Работает но добавляет в файл для отсальных тестов не очень
/*func TestSave(t *testing.T) {
//dir - получение пути каталога программы/
dir, err := os.Getwd()

if err != nil {
t.Errorf("failed to get working directory")

}
filePath := dir + "/test.json"

rep, err := NewUserRepository(filePath)
if err != nil {
t.Errorf("file direcroty is failed")
}
user := User{ID: 4, Name: "Mike"}

err = rep.Save(user)
if err != nil {
t.Errorf("save failed")
}

expected := `[{"id":0,"name":"Jonn"},{"id":1,"name":"Mark"},{"id":2,"name":"Petr"},{"id":3,"name":"Fill"},{"id":4,"name":"Mike"}]` + "\n"
text, _ := os.ReadFile("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module3/clean_architecture/repo/test.json")
if string(text) != expected {
t.Errorf("expected %q, got %q", expected, string(text))
}
}
*/

func TestUserRepository_Save(t *testing.T) {
	tempFile, err := os.CreateTemp("", "test.temp.json")
	if err != nil {
		t.Errorf("file it's not create")
	}

	defer func() {
		_ = tempFile.Close()
		_ = os.Remove(tempFile.Name())
	}()

	//rep, err := NewUserRepository(tempFile.Name())

	//if err != nil {
	//	t.Errorf("not work NewUserRep")
	//}

	test := []struct {
		name   string
		fields *os.File
		user   User
		want   string
	}{
		{
			name:   "add user 1",
			fields: tempFile,
			user:   User{ID: 0, Name: "evg"},
			want:   `[{"id":0,"name":"evg"}]` + "\n",
		},
		{
			name:   "add user 2",
			fields: tempFile,
			user:   User{ID: 1, Name: "jak"},
			want:   `[{"id":0,"name":"evg"},{"id":1,"name":"jak"}]` + "\n",
		},
	}
	for _, tt := range test {
		t.Run(tt.name, func(t *testing.T) {
			rep, err := NewUserRepository(tt.fields.Name())
			if err != nil {
				t.Error(err)
			}
			if err = rep.Save(tt.user); err != nil {
				t.Error(err)
			}
			text, _ := os.ReadFile(tt.fields.Name())

			if reflect.DeepEqual(string(text), tt.want) != true {
				t.Errorf("want %q, got %q", tt.want, string(text))
			}
		})
	}
}

func TestUserRepository_Find(t *testing.T) {
	dir, err := os.Getwd()
	if err != nil {
		t.Errorf("failed to get working directory")
	}

	filePath := dir + "/test.json"

	rep, err := NewUserRepository(filePath)
	if err != nil {
		t.Errorf("file direcroty is failed")
	}
	test := []struct {
		name    string
		field   *UserRepository
		want    User
		id      int
		wantErr error
	}{
		{
			name:    "test 1",
			field:   rep,
			want:    User{ID: 2, Name: "Petr"},
			id:      2,
			wantErr: nil,
		},
		{
			name:    "test 2",
			field:   rep,
			want:    User{},
			id:      5,
			wantErr: ErrorNotFoundUser,
		},
	}
	for _, tt := range test {
		t.Run(tt.name, func(t *testing.T) {
			got, err := rep.Find(tt.id)
			if err != nil {
				if err != tt.wantErr {
					t.Errorf("want %q, got %q", err, tt.wantErr)
				}
			} else {
				if got != tt.want {
					t.Errorf("want %q, got %q", got, tt.want)
				}
			}

		})
	}

}

func TestUserRepository_FindAll(t *testing.T) {
	dir, err := os.Getwd()
	if err != nil {
		t.Errorf("failed to get working directory")
	}
	filePath := dir + "/test.json"

	rep, err := NewUserRepository(filePath)
	if err != nil {
		t.Errorf("file direcroty is failed")
	}
	test := []struct {
		name  string
		field *UserRepository
		want  int
	}{
		{
			name:  "test 1",
			field: rep,
			want:  4,
		},
	}
	for _, tt := range test {
		t.Run(tt.name, func(t *testing.T) {
			got, _ := rep.FindAll()
			if len(got) != 4 {
				t.Errorf("want %q, got %q", got, tt.want)
			}

		})
	}

}

//Тоже работает но меняет исходный фпйл джейсон
//func BenchmarkNewUserRepositorySave(b *testing.B) {
// dir, _ := os.Getwd()
//
// filePath := dir + "/test.json"
//
// rep, _ := NewUserRepository(filePath)
// for i := 0; i < b.N; i++ {
// _ = rep.Save(User{ID: 7, Name: "Karl"})
// }
//
//}

func BenchmarkUserRepository_Find(b *testing.B) {
	dir, _ := os.Getwd()

	filePath := dir + "/test.json"

	rep, _ := NewUserRepository(filePath)
	for i := 0; i < b.N; i++ {
		_, _ = rep.Find(2)
	}
}

func BenchmarkUserRepository_FindAll(b *testing.B) {
	dir, _ := os.Getwd()

	filePath := dir + "/test.json"

	rep, _ := NewUserRepository(filePath)
	for i := 0; i < b.N; i++ {
		_, _ = rep.FindAll()
	}
}
