package repository

import (
	"encoding/json"
	"errors"
	"io"
	"os"
)

type Repository interface {
	Save(record interface{}) error
	Find(id int) (interface{}, error)
	FindAll() ([]interface{}, error)
}

type UserRepository struct {
	File *os.File
}

func NewUserRepository(filepath string) (*UserRepository, error) {
	file, err := os.OpenFile(filepath, os.O_RDWR, 0644)
	if err != nil {
		return nil, err
	}
	return &UserRepository{
		File: file,
	}, nil
}

type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

var ErrorNotFoundUser = errors.New("user with id not found")

// Загружать данные в конструкторе

//func (r *UserRepository) Save(record interface{}) error {
// // logic to write a record as a JSON object to a file at r.FilePath
// if record == nil {
// return errors.New("record is nil")
// }
// encodeJSON := json.NewEncoder(r.File)
// err := encodeJSON.Encode(record)
// if err != nil {
// return err
// }
// return nil
//}

func (r *UserRepository) Save(record interface{}) error {
	// logic to write a record as a JSON object to a file at r.FilePath
	defer func() {
		_ = r.File.Close()
	}()

	if record == nil {
		return errors.New("record is nil")
	}
	var users []User
	decoder := json.NewDecoder(r.File)

	err := decoder.Decode(&users)
	if err != nil && err != io.EOF {
		return err
	}
	user := record.(User)
	users = append(users, user)

	_ = r.File.Truncate(0)
	_, _ = r.File.Seek(0, 0)
	//

	encoder := json.NewEncoder(r.File)
	if err = encoder.Encode(&users); err != nil {
		return err
	}

	return nil
}

func (r *UserRepository) Find(id int) (interface{}, error) {
	// logic to read a JSON object from a file at r.FilePath and return the corresponding record
	decodeJSON := json.NewDecoder(r.File)
	var users []User
	for {
		err := decodeJSON.Decode(&users)
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}
		for _, user := range users {
			if user.ID == id {
				return user, nil
			}
		}
	}
	return nil, ErrorNotFoundUser
}

func (r *UserRepository) FindAll() ([]interface{}, error) {
	// logic to read all JSON objects from a file at r.FilePath and return a slice of records
	decodeJSON := json.NewDecoder(r.File)
	var users []User
	for {
		err := decodeJSON.Decode(&users)
		if err == io.EOF {
			break
		} else if err != nil {
			return nil, err
		}
	}
	res := make([]interface{}, len(users))
	for i, user := range users {
		res[i] = user
	}
	return res, nil
}
