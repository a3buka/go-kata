package main

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {
	n := &TreeNode{Val: 4, Left: &TreeNode{Val: 8, Left: &TreeNode{Val: 0}, Right: &TreeNode{Val: 1}},
		Right: &TreeNode{Val: 5, Right: &TreeNode{Val: 6}}}
	_ = averageOfSubtree(n)
}

func averageOfSubtree(root *TreeNode) int {
	ans := 0
	helper(root, &ans)
	return ans
}

func helper(node *TreeNode, n *int) []int {
	if node == nil {
		return []int{0, 0}
	}
	l := helper(node.Left, n)
	r := helper(node.Right, n)
	sum := l[0] + r[0] + node.Val
	num := l[1] + r[1] + 1
	if sum/num == node.Val {
		*n++
	}
	return []int{sum, num}
}
