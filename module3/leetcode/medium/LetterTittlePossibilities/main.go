package main

import (
	"fmt"
	"strings"
)

func main() {
	slang := "AAABBC"
	numTilePossibilities(slang)
}

func numTilePossibilities(tiles string) int {
	titlesMas := strings.Split(tiles, "")
	count := -1
	//var curMas []string
	selected := make(map[string]int, len(titlesMas))
	for num := range titlesMas {
		selected[titlesMas[num]] += 1
	}

	//used := make([]bool, len(titlesMas))
	//helper(titlesMas, &count, &used, []string{})

	//fmt.Println(temp)
	//dfs(titlesMas, curMas, &count)
	var dfs func(state []string, selected map[string]int, count *int)
	dfs = func(state []string, selected map[string]int, count *int) {

		*count++

		for v := range selected {
			if selected[v] <= 0 {
				continue
			}
			selected[v] -= 1
			dfs(append(state, v), selected, count)
			selected[v] += 1
		}
	}
	dfs([]string{}, selected, &count)

	fmt.Println(count)
	return 0
}

//func helper(tiles []string, count *int, used *[]bool, path []string) {
//	if len(path) == len(tiles) {
//		*count++
//		return
//	}
//	for i := 0; i < len(tiles); i++ {
//		if !(*used)[i] {
//			if i > 0 && tiles[i] == tiles[i-1] && !(*used)[i-1] {
//				continue
//			}
//			(*used)[i] = true
//			path = append(path, tiles[i])
//			helper(tiles, count, used, path)
//			path = path[:len(path)-1]
//			(*used)[i] = false
//		}
//
//	}
//}

//func dfs(tiles, cur []string, count *int) {
//	for i := 0; i < len(tiles); i++ {
//		if i > 0 && tiles[i] == tiles[i-1] {
//			continue
//		}
//		cur = append(cur, tiles[i])
//		*count++
//		dfs(tiles[i+1:], cur, count)
//		cur = cur[:len(cur)-1]
//	}
//}
