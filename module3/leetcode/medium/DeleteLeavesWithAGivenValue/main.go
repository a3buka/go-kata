package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {
	n := &TreeNode{1, nil, nil}
	n.Left = &TreeNode{1, nil, nil}
	//n.Left.Left = &TreeNode{2, nil, nil}

	n.Right = &TreeNode{1, nil, nil}
	//n.Right.Left = &TreeNode{2, nil, nil}
	//n.Right.Right = &TreeNode{4, nil, nil}
	nn := removeLeafNodes(n, 1)
	nn.PrintInorder()

}

func removeLeafNodes(root *TreeNode, target int) *TreeNode {
	del(root, target)
	if root.Val == target && root.Left == nil && root.Right == nil {
		root = nil
	}
	return root
}

func del(node *TreeNode, data int) *TreeNode {
	if node == nil {
		return node
	}
	if node.Left != nil {
		node.Left = del(node.Left, data)
	}
	if node.Right != nil {
		node.Right = del(node.Right, data)
	}
	if node.Val == data && node.Left == nil && node.Right == nil {
		node = nil
		return node
	}

	return node

}

//Оптимальное решение
//func removeLeafNodes(root *TreeNode, target int) *TreeNode {
//	if root == nil {
//		return root
//	}
//	root.Left = removeLeafNodes(root.Left, target)
//	root.Right = removeLeafNodes(root.Right, target)
//	if root.Left == nil && root.Right == nil && root.Val == target {
//		root = nil
//	}
//	return root
//}

func (t *TreeNode) PrintInorder() {
	if t == nil {
		return
	}

	t.Left.PrintInorder()
	t.Right.PrintInorder()
	fmt.Print(t.Val, " ")
}
