package main

import (
	"fmt"
)

type ListNode struct {
	Val  int
	Next *ListNode
}

func main() {
	r := &ListNode{Val: 5, Next: nil}
	e := &ListNode{Val: 4, Next: r}
	d := &ListNode{Val: 3, Next: e}
	c := &ListNode{Val: 2, Next: d}
	b := &ListNode{Val: 1, Next: c}
	a := &ListNode{Val: 0, Next: b}

	nn := &ListNode{Val: 100}
	nn.Next = &ListNode{Val: 101}
	nn.Next.Next = &ListNode{Val: 102}
	z := 3
	x := 4
	list := mergeInBetween(a, z, x, nn)
	fmt.Println(list)

}

func (l *ListNode) String() string {
	if l == nil {
		return "nil"
	}
	if l.Next == nil {
		return fmt.Sprintf("%d", l.Val)
	}
	return fmt.Sprintf("%d -> %s", l.Val, l.Next)
}

func mergeInBetween(list1 *ListNode, a int, b int, list2 *ListNode) *ListNode {
	num := make([]int, 0, a)
	for i := 0; i <= b; i++ {
		if i < a {
			num = append(num, list1.Val)
		}
		list1 = list1.Next
	}
	for list2 != nil {
		num = append(num, list2.Val)
		list2 = list2.Next
	}
	for list1 != nil {
		num = append(num, list1.Val)
		list1 = list1.Next
	}
	head := &ListNode{Val: num[0]}
	head = newList(head, num)
	fmt.Println(head)
	return head
}

func newList(head *ListNode, num []int) *ListNode {
	temp := head
	for i := 1; i < len(num); i++ {
		temp.Next = &ListNode{Val: num[i]}
		temp = temp.Next
	}
	return head
}

//Оптимальное решение
//func mergeInBetween(list1 *ListNode, a int, b int, list2 *ListNode) *ListNode {
//	n := list1
//	var startRef, endRef *ListNode
//	for i := 0; i <= b; i++ {
//		if i == a-1 {
//			startRef = n
//		}
//		if i == b {
//			endRef = n
//		}
//		n = n.Next
//	}
//	startRef.Next = list2
//	n = list2
//	for n.Next != nil {
//		n = n.Next
//	}
//	n.Next = endRef.Next
//	return list1
//}
