package main

import (
	"fmt"
)

func main() {
	n := "323"
	k := minPartitions(n)
	fmt.Println(k)
}

func minPartitions(n string) int {
	var max int32

	for _, count := range n {
		if count > max {
			max = count
		}
	}
	//res, _ := strconv.Atoi(string(max))
	return int(max - '0')

}
