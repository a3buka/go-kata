package main

import "fmt"

func main() {
	test := [][]int{{1, 2, 1}, {4, 3, 4}, {3, 2, 1}, {1, 1, 1}}
	obj := Constructor(test)
	cow := obj.GetValue(0, 2)
	fmt.Println(cow)
	obj.UpdateSubrectangle(0, 0, 3, 2, 5)
	cow = obj.GetValue(0, 2)
	fmt.Println(cow)
}

type SubrectangleQueries struct {
	res [][]int
}

func Constructor(rectangle [][]int) SubrectangleQueries {
	return SubrectangleQueries{res: rectangle}
}

func (this *SubrectangleQueries) UpdateSubrectangle(row1 int, col1 int, row2 int, col2 int, newValue int) {
	for i := row1; i <= row2; i++ {
		for j := col1; j <= col2; j++ {
			this.res[i][j] = newValue
		}
	}
}

func (this *SubrectangleQueries) GetValue(row int, col int) int {
	return this.res[row][col]
}
