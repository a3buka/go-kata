package main

import (
	"fmt"
)

type ListNode struct {
	Val  int
	Next *ListNode
}

func main() {
	n := &ListNode{Val: 0}
	n.Next = &ListNode{Val: 3}
	n.Next.Next = &ListNode{Val: 1}
	n.Next.Next.Next = &ListNode{Val: 0}
	n.Next.Next.Next.Next = &ListNode{Val: 5}
	n.Next.Next.Next.Next.Next = &ListNode{Val: 0}

	nn := mergeNodes(n)
	i := 0
	for nn != nil {
		fmt.Printf("%d => %v\n", i, nn.Val)
		i++
		nn = nn.Next
	}
	//fmt.Println(l)
}

func mergeNodes(head *ListNode) *ListNode {
	if head == nil || head.Val == 0 && head.Next == nil {
		return nil
	}
	curr := head
	empty := &ListNode{}
	//count := 0
	for curr.Next != nil {
		if curr.Val == 0 {
			empty = curr
			curr = curr.Next

		} else {
			empty.Val += curr.Val
			*curr = *curr.Next

		}
		//curr = curr.Next
	}
	empty.Next = nil

	return head
}
