package main

import (
	"fmt"
	"sort"
)

func main() {
	nums := []int{1, 2, 2}
	subsetsWithDup(nums)
}

func subsetsWithDup(nums []int) [][]int {
	array := [][]int{[]int{}}
	var cur []int
	sort.Ints(nums)
	//array[0][0] = nil
	doSubsetsWithDup(nums, cur, &array)
	fmt.Println(array)
	return array

}

func doSubsetsWithDup(nums []int, cur []int, res *[][]int) {
	for i := 0; i < len(nums); i++ {
		if i > 0 && nums[i] == nums[i-1] {
			continue
		}
		cur = append(cur, nums[i])
		newRes := make([]int, len(cur))
		copy(newRes, cur)
		*res = append(*res, newRes)
		doSubsetsWithDup(nums[i+1:], cur, res)
		cur = cur[:len(cur)-1]

	}
}
