package main

import (
	"fmt"
)

type TreeNode struct {
	value int
	left  *TreeNode
	right *TreeNode
}

func main() {
	nums := []int{3, 2, 1, 6, 0, 5}
	n := constructMaximumBinaryTree(nums)
	n.PrintInorder()
}

// Решение Номер 1
func constructMaximumBinaryTree(nums []int) *TreeNode {
	if len(nums) == 0 {
		return nil
	}
	//var max, count int
	max := -1
	count := -1
	for i, items := range nums {
		if items > max {
			max = items
			count = i
		}
	}
	node := &TreeNode{value: max}
	//sort.Ints(nums[:count])
	left := constructMaximumBinaryTree(nums[:count])
	right := constructMaximumBinaryTree(nums[count+1:])
	node.left = left
	node.right = right
	//for j := len(nums[:count]) - 1; j >= 0; j-- {
	//	node.left = AddVote(node, &nums[j])
	//}
	return node
}

// Решение номер 2 не совсем верно
//func constructMaximumBinaryTree(nums []int) *TreeNode {
//	if len(nums) == 0 {
//		return nil
//	}
//	//var max, count int
//	max := -1
//	count := -1
//	for i, items := range nums {
//		if items > max {
//			max = items
//			count = i
//		}
//	}
//	node := &TreeNode{value: max}
//	sort.Ints(nums[:count])
//
//	for j := len(nums[:count]) - 1; j >= 0; j-- {
//		AddVoteLeft(node, &nums[j])
//	}
//
//	sort.Ints(nums[count+1:])
//	for j := len(nums) - 1; j > len(nums[:count]); j-- {
//		AddVoteRight(node, &nums[j])
//	}
//
//	return node
//
//}
//
//func AddVoteLeft(node *TreeNode, val *int) {
//	if node.left == nil {
//		node.left = &TreeNode{value: *val}
//	} else {
//		AddVoteLeft(node.left, val)
//	}
//}
//
//func AddVoteRight(node *TreeNode, val *int) {
//	if node.right == nil {
//		node.right = &TreeNode{value: *val}
//	} else {
//		AddVoteRight(node.right, val)
//	}
//}

func (t *TreeNode) PrintInorder() {
	if t == nil {
		return
	}

	t.left.PrintInorder()
	t.right.PrintInorder()
	fmt.Print(t.value, " ")
}
