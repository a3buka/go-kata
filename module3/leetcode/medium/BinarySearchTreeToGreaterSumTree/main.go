package main

import (
	"fmt"
)

type TreeNode struct {
	value int
	left  *TreeNode
	right *TreeNode
}

func main() {
	n := &TreeNode{4, nil, nil}
	n.left = &TreeNode{1, nil, nil}
	n.right = &TreeNode{6, nil, nil}
	n.right.right = &TreeNode{7, nil, nil}
	n.right.left = &TreeNode{5, nil, nil}
	n.right.right.right = &TreeNode{8, nil, nil}
	n.left.left = &TreeNode{0, nil, nil}
	n.left.right = &TreeNode{2, nil, nil}
	n.left.left.left = &TreeNode{3, nil, nil}
	//n.PrintInorder()
	n = bstToGst(n)

	n.PrintInorder()

}

func (t *TreeNode) PrintInorder() {
	if t == nil {
		return
	}

	t.left.PrintInorder()
	t.right.PrintInorder()
	fmt.Print(t.value, " ")
}

func bstToGst(root *TreeNode) *TreeNode {
	if root == nil {
		return nil
	}
	count := 0
	addVote(root, &count)
	return root
}

func addVote(root *TreeNode, count *int) {

	if root == nil {
		return
	}
	//if root.right != nil {
	//	*count += root.right.value
	//
	//}
	addVote(root.right, count)
	*count = *count + root.value
	root.value = *count

	addVote(root.left, count)

}
