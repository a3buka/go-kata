package main

import "fmt"

func main() {
	arr := []int{1, 3, 4, 8}
	//arr := []int{16}

	queries := [][]int{{0, 1}, {1, 2}, {0, 3}, {3, 3}}
	//queries := [][]int{{0, 0}, {0, 0}, {0, 0}}

	m := xorQueries(arr, queries)
	fmt.Println(m)
	//fmt.Println(1 ^ 3 ^ 4 ^ 8)
}

//func xorQueries(arr []int, queries [][]int) []int {
//	//res := make([]int, len(arr))
//	var res []int
//	count := 0
//	for _, item := range queries {
//		count = arr[item[0]]
//		for j := item[0]; j < item[1]; j++ {
//
//			count = count ^ arr[j+1]
//		}
//		res = append(res, count)
//		//
//	}
//	return res
//}

// идеальное
func xorQueries(arr []int, queries [][]int) []int {
	temp := 0
	prefix := make([]int, len(arr))
	n := len(arr)

	for i := 0; i < n; i++ {
		temp = temp ^ arr[i]
		prefix[i] = temp
	}

	var ans []int = make([]int, len(queries))
	counter := 0
	for _, q := range queries {
		res := prefix[q[1]]
		if q[0] != 0 {
			res = res ^ prefix[q[0]-1]
		}
		ans[counter] = res
		counter++
	}

	return ans
}
