package main

import "fmt"

func main() {
	exercise := []int{3, 1, 2, 1}
	processQueries(exercise, 5)
}

func processQueries(queries []int, m int) []int {
	temp := make([]int, 0, m)
	res := make([]int, 0, len(queries))
	//newtemp := make([]int, 0, m)
	for i := 1; i <= m; i++ {
		temp = append(temp, i)
	}
	for _, idx := range queries {
		for y, idy := range temp {
			if idx == idy {
				res = append(res, y)
				perm(temp, y)
			}
		}
	}
	fmt.Println(res)
	return res
}

func perm(temp []int, pos int) {
	first := temp[pos]
	for i := pos; i > 0; i-- {
		temp[i] = temp[i-1]
	}
	temp[0] = first
}
