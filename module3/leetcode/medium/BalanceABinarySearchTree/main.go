package main

import "fmt"

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func main() {
	tree := &TreeNode{Val: 1}
	tree.Right = &TreeNode{Val: 2}
	tree.Right.Right = &TreeNode{Val: 3}
	tree.Right.Right.Right = &TreeNode{Val: 4}
	//tree.Left = &TreeNode{Val: 4}
	//tree.PrintInorder()
	newTree := balanceBST(tree)
	newTree.PrintInorder()

}

func balanceBST(root *TreeNode) *TreeNode {
	//создаем массив из root
	var massiveTree []int
	createMassive(root, &massiveTree)

	//создаем из массива сбалансированное дерево
	//newTree := &TreeNode{}
	//newTree := c
	return createTree(massiveTree, 0, len(massiveTree)-1)
}

func createMassive(root *TreeNode, n *[]int) {
	if root == nil {
		return
	}

	createMassive(root.Left, n)
	*n = append(*n, root.Val)
	createMassive(root.Right, n)
}

func createTree(n []int, first, last int) *TreeNode {
	if first > last {
		return nil
	}
	middle := first + (last-first)/2
	root := TreeNode{Val: n[middle]}
	root.Left = createTree(n, first, middle-1)
	root.Right = createTree(n, middle+1, last)
	return &root
}

//Решение не зациклено а так же работает только на левые и правые доечрние эелементы
//func balanceBST(root *TreeNode) *TreeNode {
//
//	switch {
//	case root.Left == nil && root.Right.Right != nil:
//		root = rotateRight(root)
//		return root
//	case root.Right == nil && root.Left.Left != nil:
//		root = rotateLeft(root)
//		return root
//	}
//	return root
//
//}

//func rotateLeft(t *TreeNode) *TreeNode {
//	l := t.Left
//	t.Left = l.Right
//	l.Right = t
//	return l
//}
//
//func rotateRight(t *TreeNode) *TreeNode {
//	r := t.Right
//	t.Right = r.Left
//	r.Left = t
//	return r
//}

func (t *TreeNode) PrintInorder() {
	if t == nil {
		return
	}

	t.Left.PrintInorder()
	t.Right.PrintInorder()
	fmt.Print(t.Val, " ")
}

//Очень интересное решение
//
//func balance(nodes []*TreeNode) *TreeNode {
//	n := len(nodes)
//	if n == 0 { return nil }
//	if n == 1 {
//		nodes[0].Left, nodes[0].Right = nil, nil
//		return nodes[0]
//	}
//	newRoot := nodes[n/2]
//	newRoot.Left = balance(nodes[:n/2])
//	newRoot.Right = balance(nodes[n/2+1:])
//	return newRoot
//}
//
//func dfs(node *TreeNode, nodes *[]*TreeNode) {
//	if node == nil { return }
//	if node.Left != nil {
//		dfs(node.Left, nodes)
//	}
//	*nodes = append(*nodes, node)
//	if node.Right != nil {
//		dfs(node.Right, nodes)
//	}
//}
