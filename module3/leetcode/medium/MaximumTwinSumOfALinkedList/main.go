package main

import "fmt"

type ListNode struct {
	Val  int
	Next *ListNode
}

func main() {
	n := &ListNode{Val: 1}
	n.Next = &ListNode{Val: 1000}
	//n.Next.Next = &ListNode{Val: 2}
	//n.Next.Next.Next = &ListNode{Val: 1}
	fmt.Println(pairSum(n))

}

func pairSum(head *ListNode) int {
	//var sliceValue map[int]int
	var slice []int
	needLen := lenNode(head)
	value := 0
	max := 0
	j := 0
	for i := 0; i < needLen; i++ {
		if i < needLen/2 {
			slice = append(slice, head.Val)

		}
		if i >= needLen/2 {
			j++
			value = slice[needLen/2-j] + head.Val

		}
		if value > max {
			max = value
		}

		head = head.Next
	}
	return max
}

func lenNode(head *ListNode) int {
	LenNode := head
	i := 0
	for LenNode != nil {
		i++
		LenNode = LenNode.Next
	}
	return i
}
