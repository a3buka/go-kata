package main

func main() {
	groupPeople := []int{3, 3, 3, 3, 3, 1, 3}
	_ = groupThePeople(groupPeople)
}

func groupThePeople(groupSizes []int) [][]int {
	res := [][]int{}
	temp := make(map[int][]int, 6)

	for i := range groupSizes {
		if len(temp[groupSizes[i]]) < groupSizes[i] {
			temp[groupSizes[i]] = append(temp[groupSizes[i]], i)
		} else {
			res = append(res, temp[groupSizes[i]])
			temp[groupSizes[i]] = []int{}
			temp[groupSizes[i]] = append(temp[groupSizes[i]], i)
		}

	}
	for _, idx := range temp {
		res = append(res, idx)
	}

	return res
}
