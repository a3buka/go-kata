package main

import "fmt"

func main() {
	//var score [3][3]int = {{}, {}}
	score := [][]int{{10, 6, 9, 1}, {7, 5, 11, 2}, {4, 8, 3, 15}}
	k := 2
	sortTheStudents(score, k)
	fmt.Println(score)
}

func sortTheStudents(score [][]int, k int) [][]int {
	//sortScore := make([][]int, len(score))
	i := 1
	swaped := true
	for swaped {
		swaped = false
		for j := 0; j < len(score)-i; j++ {
			if score[j][k] < score[j+1][k] {
				score[j], score[j+1] = score[j+1], score[j]
				swaped = true
			}
		}
		i++
	}
	return score
}
