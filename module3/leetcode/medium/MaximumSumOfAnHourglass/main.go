package main

func main() {
	num := [][]int{{520626, 685427, 788912, 800638, 717251, 683428}, {23602, 608915, 697585, 957500, 154778, 209236}, {287585, 588801, 818234, 73530, 939116, 252369}}
	maxSum(num)

}

func maxSum(grid [][]int) int {
	var count, max int
	for j := 0; j < len(grid)-2; j++ {
		for i := 0; i < len(grid[j])-2; i++ {
			count = grid[j][i] + grid[j][i+1] + grid[j][i+2] + grid[j+1][i+1] + grid[j+2][i] + grid[j+2][i+1] + grid[j+2][i+2]
			if count > max {
				max = count
			}
		}
	}
	return max
}
