package main

import "fmt"

func main() {
	pointers := [][]int{{1, 3}, {3, 3}, {5, 3}, {2, 2}}
	quires := [][]int{{2, 3, 1}, {4, 3, 1}, {1, 1, 2}}
	res := countPoints(pointers, quires)
	fmt.Println(res)
}

func countPoints(points [][]int, queries [][]int) []int {
	temp := make([]int, 0, len(queries))

	for i := 0; i < len(queries); i++ {
		count := 0
		x := queries[i][0]
		y := queries[i][1]
		r := queries[i][2] * queries[i][2]
		for j := 0; j < len(points); j++ {

			x0 := points[j][0]
			y0 := points[j][1]
			z := (x-x0)*(x-x0) + (y-y0)*(y-y0)

			if z <= r {
				count++
			}
		}
		temp = append(temp, count)
	}
	return temp
}
