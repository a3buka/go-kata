package main

import (
	"fmt"
)

func main() {

	edges := [][]int{{1, 2}, {3, 2}, {4, 1}, {3, 4}, {0, 2}}
	n := 6
	answer := findSmallestSetOfVertices(n, edges)
	fmt.Println(answer)

}

type TreeNode struct {
	val   int
	right *TreeNode
	left  *TreeNode
}

func findSmallestSetOfVertices(n int, edges [][]int) []int {
	//m := make(map[int]int, n)
	//firstValue := make([]int, 0, n)
	//secondValue := make([]int, 0, n)
	valueNode := &TreeNode{val: edges[0][0]}

	for _, item := range edges {
		insert(valueNode, item[0])
	}

	for _, item := range edges {
		valueNode = del(valueNode, item[1])
	}

	answer := make([]int, 0, n)
	addInSlice(valueNode, &answer)

	return answer
}

func insert(n *TreeNode, data int) *TreeNode {
	if n == nil {
		return nil
	} else if data < n.val {
		if n.left == nil {
			n.left = &TreeNode{val: data, left: nil, right: nil}
		} else {
			n = insert(n.left, data)
		}
	} else if data > n.val {
		if n.right == nil {
			n.right = &TreeNode{val: data, left: nil, right: nil}
		} else {
			n = insert(n.right, data)
		}
	}
	return n
}

func del(node *TreeNode, data int) *TreeNode {
	if node == nil {
		return node
	}
	if node.val > data {
		node.left = del(node.left, data)
	}
	if node.val < data {
		node.right = del(node.right, data)
	}
	if node.val == data {
		if node.left == nil && node.right == nil {
			node = nil
			return node
		}
		if node.left == nil && node.right != nil {
			temp := node.right
			node = nil
			node = temp
			return node
		}
		if node.left != nil && node.right == nil {
			temp := node.left
			node = nil
			node = temp
			return node
		}
		tempNode := minValued(node.right)
		node.val = tempNode.val
		node.right = del(node.right, tempNode.val)

	}
	return node
}

func minValued(root *TreeNode) *TreeNode {
	temp := root
	for nil != temp && temp.left != nil {
		temp = temp.left
	}
	return temp
}

func addInSlice(node *TreeNode, n *[]int) {

	*n = append(*n, node.val)
	if node.left != nil {
		addInSlice(node.left, n)
	}
	if node.right != nil {
		addInSlice(node.right, n)
	}

}

//func (n *TreeNode) insert(data int) {
//	if n == nil {
//		return
//	} else if data < n.val {
//		if n.left == nil {
//			n.left = &TreeNode{val: data, left: nil, right: nil}
//		} else {
//			n.left.insert(data)
//		}
//	} else if data > n.val {
//		if n.right == nil {
//			n.right = &TreeNode{val: data, left: nil, right: nil}
//		} else {
//			n.right.insert(data)
//		}
//	}
//}
