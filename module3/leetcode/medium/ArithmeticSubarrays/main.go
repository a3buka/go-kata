package main

import (
	"fmt"
	"sort"
)

func main() {
	//var nums, l, r []int
	nums := []int{4, 6, 5, 9, 3, 7}
	l := []int{0, 0, 2}
	r := []int{2, 3, 5}

	result := checkArithmeticSubarrays(nums, l, r)
	fmt.Println(result)

}

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	res := []bool{}

	for i := 0; i < len(l); i++ {
		pass := make([]int, len(nums[l[i]:r[i]+1]))
		copy(pass, nums[l[i]:r[i]+1])
		//pass = nums[l[i] : r[i]+1]
		sort.Ints(pass)

		swaped := true
		//for j := range nums[l[i]+1 : r[i]+1] {
		for j := 0; j < r[i]-l[i]; j++ {
			if pass[0]-pass[1] != pass[j]-pass[j+1] {
				swaped = false
			}
		}
		res = append(res, swaped)
	}
	return res
}
