package main

import "fmt"

func main() {
	n := []int{1, 1, 2}
	permuteUnique(n)
}

func permuteUnique(nums []int) [][]int {
	solutions := [][]int{}

	selected := map[int]int{}
	for _, num := range nums {
		selected[num] += 1
	}

	var dfs func(state []int, selected map[int]int)
	dfs = func(state []int, selected map[int]int) {
		if len(state) == len(nums) {
			cpy := make([]int, len(state))
			copy(cpy, state)
			solutions = append(solutions, cpy)
			return
		}

		for v := range selected {
			if selected[v] <= 0 {
				continue
			}
			selected[v] -= 1
			dfs(append(state, v), selected)
			selected[v] += 1
		}
	}

	dfs([]int{}, selected)
	fmt.Println(solutions)
	return solutions
}

//Еще один алгоритм решения
//func permuteUnique(nums []int) [][]int {
//sort.Ints(nums)
//used := make([]bool, len(nums))
//result := make([][]int, 0)
//helper(nums, &result, &used, []int{})
//return result
//}
//
//func helper(nums []int, res *[][]int, used *[]bool, path []int) {
//	if len(path) == len(nums) {
//		copied := make([]int, len(path))
//		copy(copied, path)
//		*res = append(*res, copied)
//		return
//	}
//	for i := 0; i < len(nums); i++ {
//		if !(*used)[i] {
//			if i > 0 && nums[i] == nums[i-1] && !(*used)[i-1] {
//				continue
//			}
//			(*used)[i] = true
//			path = append(path, nums[i])
//			helper(nums, res, used, path)
//			path = path[:len(path)-1]
//			(*used)[i] = false
//		}
//	}
//}
