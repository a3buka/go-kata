package main

import "fmt"

type node struct {
	value int
	left  *node
	right *node
}

//type bst struct {
//	root *node
//	len  int
//}

func main() {
	n := &node{1, nil, nil}
	n.left = &node{2, nil, nil}
	n.right = &node{4, nil, nil}
	n.right.right = &node{6, nil, nil}
	n.right.right.right = &node{8, nil, nil}
	n.left.left = &node{4, nil, nil}
	n.left.right = &node{5, nil, nil}
	n.left.left.left = &node{10, nil, nil}
	//b := bst{
	//	root: n,
	//	len:  4,
	//}
	//fmt.Println(b)
	fmt.Println(deepestLeavesSum(n))
}

var value, maxI int

func deepestLeavesSum(n *node) int {

	if n == nil {
		return 0
	}
	value = 0
	maxI = 0
	rootValue(n, 0)
	return value
}

func rootValue(n *node, i int) {
	if n == nil {
		return
	}

	if maxI < i {
		maxI = i
		value = 0
	}
	if maxI == i {
		value += n.value
	}

	rootValue(n.left, i+1)
	rootValue(n.right, i+1)
}
