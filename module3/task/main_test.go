package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func initLinkedListData() *DoubleLinkedList {
	f := new(DoubleLinkedList)

	data := Commit{Message: "We need to program the bluetooth ADP protocol!",
		UUID: "6956d3f4-875b-11ed-8150-acde48001122",
		Date: time.Date(2002, time.November, 10, 23, 0, 0, 0, time.UTC)}
	f.Append(data)

	data = Commit{Message: "commit number2!",
		UUID: "8236d3f4-875b-11ed-8150-acde48001122",
		Date: time.Date(1995, time.September, 17, 23, 0, 0, 0, time.UTC)}
	f.Append(data)

	data = Commit{Message: "commit number3!",
		UUID: "8236d4a4-875b-11ed-8150-acde48001122",
		Date: time.Date(2005, time.December, 17, 23, 0, 0, 0, time.UTC)}
	f.Append(data)

	return f
}

func initLinkedListDataEmpty() *DoubleLinkedList {
	p := new(DoubleLinkedList)
	return p
}

func TestDoubleLinkedList_DeleteCurrent(t *testing.T) {
	cases := []struct {
		name     string
		in       *DoubleLinkedList
		expError error
	}{
		{
			name:     "LinkedList is empty",
			in:       initLinkedListDataEmpty(),
			expError: ErrListIsEmpty,
		},
		{
			name:     "Linked list is allowed",
			in:       initLinkedListData(),
			expError: nil,
		},
	}

	for _, tCase := range cases {
		t.Run(tCase.name, func(t *testing.T) {
			err := tCase.in.DeleteCurrent()
			if err != nil {
				require.Error(t, err)
				require.EqualError(t, tCase.expError, err.Error())
			} else {
				require.NoError(t, err)
			}
		})
	}

}

func TestDoubleLinkedList_Delete(t *testing.T) {

	cases := []struct {
		name   string
		in     *DoubleLinkedList
		n      int
		errExp error
	}{

		{
			name:   "deleted: when number >= len",
			in:     initLinkedListData(),
			n:      4,
			errExp: ErrOutOfRange,
		},
		{
			name:   "deleted: when number < 0",
			in:     initLinkedListData(),
			n:      -1,
			errExp: ErrOutOfRange,
		},
		{
			name:   "deleted normal number",
			in:     initLinkedListData(),
			n:      0,
			errExp: nil,
		},
		{
			name:   "Linked List is empty",
			in:     initLinkedListDataEmpty(),
			n:      3,
			errExp: ErrListIsEmpty,
		},
	}
	for _, tCase := range cases {
		t.Run(tCase.name, func(t *testing.T) {
			err := tCase.in.Delete(tCase.n)
			if err != nil {
				require.Error(t, err)
				require.EqualError(t, tCase.errExp, err.Error())
			} else {
				require.NoError(t, err)
			}

		})
	}
}

func TestDoubleLinkedList_Insert(t *testing.T) {
	cases := []struct {
		name   string
		in     *DoubleLinkedList
		n      int
		errExp error
	}{

		{
			name:   "Add 1 elem in 0pos",
			in:     initLinkedListDataEmpty(),
			n:      0,
			errExp: nil,
		},
		{
			name:   "Add 1 elem in 1pos",
			in:     initLinkedListDataEmpty(),
			n:      1,
			errExp: nil,
		},
		{
			name:   "Add 1 elem in 2pos",
			in:     initLinkedListDataEmpty(),
			n:      2,
			errExp: nil,
		},
		{
			name:   "Add 1 elem in 3pos",
			in:     initLinkedListDataEmpty(),
			n:      3,
			errExp: nil,
		},
		{
			name:   "Add 1 elem when n < 0",
			in:     initLinkedListDataEmpty(),
			n:      -3,
			errExp: ErrOutOfRange,
		},
	}
	for _, tCase := range cases {
		t.Run(tCase.name, func(t *testing.T) {
			err := tCase.in.Insert(tCase.n, Commit{
				Message: "add new commit",
				Date:    time.Date(2012, time.October, 11, 12, 30, 20, 0, time.UTC),
				UUID:    "1236d3f4-875b-11ed-8150-acde48001122"})
			if err != nil {
				require.Error(t, err)
				require.EqualError(t, tCase.errExp, err.Error())
			} else {
				require.NoError(t, err)
			}
		})
	}
}

func TestDoubleLinkedList_Index_error(t *testing.T) {
	cases := []struct {
		name     string
		in       *DoubleLinkedList
		expError error
	}{
		{
			name:     "LinkedList is empty",
			in:       initLinkedListDataEmpty(),
			expError: ErrListIsEmpty,
		},
		{
			name:     "Linked list is allowed",
			in:       initLinkedListData(),
			expError: nil,
		},
	}

	for _, tCase := range cases {
		t.Run(tCase.name, func(t *testing.T) {
			_, err := tCase.in.Index()
			if err != nil {
				require.Error(t, err)
				require.EqualError(t, tCase.expError, err.Error())
			} else {
				require.NoError(t, err)
			}

		})
	}

}

func TestDoubleLinkedList_Index_ReturnInt(t *testing.T) {
	cases := []struct {
		name string
		in   *DoubleLinkedList
		n    int
	}{
		{
			name: "LinkedList is empty",
			in:   initLinkedListDataEmpty(),
			n:    -1,
		},
		{
			name: "Linked list is allowed",
			in:   initLinkedListData(),
			n:    1,
		},
	}

	for _, tCase := range cases {
		t.Run(tCase.name, func(t *testing.T) {
			n, _ := tCase.in.Index()
			if n != tCase.n {
				t.Errorf("Index = %v, recive index = %v", tCase.n, n)
			}

		})
	}

}

func BenchmarkDoubleLinkedList_DeleteCurrent(b *testing.B) {
	l := initLinkedListData()
	for i := 0; i < b.N; i++ {
		_ = l.DeleteCurrent()
	}
}

func BenchmarkDoubleLinkedList_Delete(b *testing.B) {
	l := initLinkedListData()
	for i := 0; i < b.N; i++ {
		_ = l.Delete(2)
	}
}

func benchmarkDoubleLinkedListInsert(b *testing.B, n int) {

	for i := 0; i < b.N; i++ {
		l := initLinkedListDataEmpty()
		_ = l.Insert(n, Commit{
			Message: "add new commits",
			Date:    time.Date(2011, time.October, 11, 12, 30, 20, 0, time.UTC),
			UUID:    "1236d3f4-875b-11ed-8150-acde48001122"})

	}

}

func Benchmark1Insert(b *testing.B) {
	benchmarkDoubleLinkedListInsert(b, 1)
}
func BenchmarkDoubleLinkedList_Index(b *testing.B) {
	l := initLinkedListData()
	for i := 0; i < b.N; i++ {
		_, _ = l.Index()
	}
}
