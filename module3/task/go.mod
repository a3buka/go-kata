module gitlab.com/a3buka/go-kata/module3/task

go 1.19

require (
	github.com/brianvoe/gofakeit/v6 v6.20.1
	github.com/stretchr/testify v1.8.1
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
