package main

import "fmt"

type PricingStrategy interface {
	Calculate(order Order) float64
}

type regularPricing struct {
}

type salePricing struct {
	discount float64
}

type Order struct {
	name     string
	price    float64
	quantity float64
}

func (r regularPricing) Calculate(order Order) float64 {
	return order.price * order.quantity
}

func (s salePricing) Calculate(order Order) float64 {
	return order.price * (1 - s.discount/100) * order.quantity
}

func NewOrder() *Order {
	return &Order{name: "apple", price: 7, quantity: 4}
}

func check(order *Order, strategy PricingStrategy) float64 {
	return strategy.Calculate(*order)
}

func main() {
	apple := NewOrder()
	inter := []PricingStrategy{regularPricing{}, salePricing{5}, salePricing{10}, salePricing{15}}
	for i := range inter {
		price := check(apple, inter[i])
		if i == 0 {
			fmt.Printf("Total cost with discount 0 strategy: %v\n", price)
			continue
		}
		fmt.Printf("Total cost with %#v strategy: %v\n", inter[i], price)
	}
}
