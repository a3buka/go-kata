package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"strconv"
)

// WeatherAPI is the interface that defines the methods for accessing weather information
type WeatherAPI interface {
	GetTemperature(location string) int
	GetHumidity(location string) int
	GetWindSpeed(location string) float64
}

// OpenWeatherAPI is the implementation of the weather API
type OpenWeatherAPI struct {
	apiKey string
}

type data struct {
	Country string  `json:"country"`
	Lat     float64 `json:"lat"`
	Lon     float64 `json:"lon"`
}

type Weather struct {
	Main struct {
		Temp     float64 `json:"temp"`
		Humidity float64 `json:"humidity"`
	} `json:"main"`
	Wind struct {
		Speed float64 `json:"speed"`
	}
}

func check(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func (o *OpenWeatherAPI) GetWeatherRequest(location string) *Weather {
	res, _ := http.Get("https://api.openweathermap.org/geo/1.0/direct?q=" + location + "&limit=5&appid=" + o.apiKey)
	b, _ := io.ReadAll(res.Body)
	var dataCode []data
	var resWeather Weather
	_ = json.Unmarshal(b, &dataCode)

	for i := range dataCode {
		if dataCode[i].Country == "RU" {
			rep, err := http.Get("https://api.openweathermap.org/data/2.5/weather?lat=" + strconv.FormatFloat(dataCode[i].Lat, 'f', 4, 64) + "&lon=" + strconv.FormatFloat(dataCode[i].Lon, 'f', 2, 64) + "&appid=" + o.apiKey)
			check(err)
			repByte, err := io.ReadAll(rep.Body)
			check(err)
			err = json.Unmarshal(repByte, &resWeather)
			err = rep.Body.Close()
			check(err)

		}
	}
	defer res.Body.Close()

	return &resWeather
}

func (o *OpenWeatherAPI) GetTemperature(location string) int {
	// Make a request to the open weather API to retrieve temperature information
	// and return the result
	return int(o.GetWeatherRequest(location).Main.Temp - 272)
}

func (o *OpenWeatherAPI) GetHumidity(location string) int {
	// Make a request to the open weather API to retrieve humidity information
	// and return the result
	// ...

	return int(o.GetWeatherRequest(location).Main.Humidity)
}

func (o *OpenWeatherAPI) GetWindSpeed(location string) float64 {
	// Make a request to the open weather API to retrieve wind speed information
	// and return the result
	// ...

	return o.GetWeatherRequest(location).Wind.Speed
}

// WeatherFacade is the facade that provides a simplified interface to the weather API
type WeatherFacade struct {
	weatherAPI WeatherAPI
}

func (w *WeatherFacade) GetWeatherInfo(location string) (int, int, float64) {
	temperature := w.weatherAPI.GetTemperature(location)
	humidity := w.weatherAPI.GetHumidity(location)
	windSpeed := w.weatherAPI.GetWindSpeed(location)

	return temperature, humidity, windSpeed
}

func NewWeatherFacade(apiKey string) WeatherFacade {
	return WeatherFacade{
		weatherAPI: &OpenWeatherAPI{apiKey: apiKey},
	}
}

func main() {
	weatherFacade := NewWeatherFacade("d0eb214a9191da20c8c425e6df905908")
	cities := []string{"Москва", "Saint Petersburg", "Казань", "Якутск"}

	for _, city := range cities {
		temperature, humidity, windSpeed := weatherFacade.GetWeatherInfo(city)
		fmt.Printf("Temperature in "+city+": %d\n", temperature)
		fmt.Printf("Humidity in "+city+": %d\n", humidity)
		fmt.Printf("Wind speed in "+city+": %.2f\n\n", windSpeed)
	}
}
