package main

import "fmt"

// AirConditioner is the interface that defines the methods for controlling the air conditioner
type AirConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature(temp int)
}

// RealAirConditioner is the implementation of the air conditioner
type RealAirConditioner struct{}

func (os *RealAirConditioner) TurnOn() {
	fmt.Println("Turning on the air conditioner")
}

func (os *RealAirConditioner) TurnOff() {
	fmt.Println("Turning off the air conditioner")
}

func (os *RealAirConditioner) SetTemperature(temp int) {
	fmt.Printf("Setting air conditioner temperature to %v\n", temp)
}

type AirConditionerAdapter struct {
	AirConditioner *RealAirConditioner
}

type AirConditionerProxy struct {
	adapter *AirConditionerAdapter
	auth    bool
}

func (p *AirConditionerProxy) TurnOn() {
	if !p.auth {
		return
	}
	p.adapter.AirConditioner.TurnOn()

}

func (p *AirConditionerProxy) TurnOff() {
	if !p.auth {
		return
	}
	p.adapter.AirConditioner.TurnOff()

}

func (p *AirConditionerProxy) SetTemperature(temp int) {
	if !p.auth {
		return
	}
	p.adapter.AirConditioner.SetTemperature(temp)

}

func NewProxy(auth bool) AirConditionerProxy {
	if !auth {
		fmt.Println("access denied")
		return AirConditionerProxy{auth: false}
	}
	rls := &RealAirConditioner{}
	res := &AirConditionerAdapter{rls}
	fmt.Println("authentication required to turn on the air conditioner")
	fmt.Println("authentication required to turn off the air conditioner")
	fmt.Println("authentication required to set the temperature of the air conditioner")
	return AirConditionerProxy{adapter: res, auth: true}
}

func main() {
	testAgree := NewProxy(true)
	testAgree.TurnOn()
	testAgree.TurnOff()
	testAgree.SetTemperature(25)

	testAgree1 := NewProxy(false)
	testAgree1.TurnOn()
	testAgree1.TurnOff()
	testAgree1.SetTemperature(30)
}
