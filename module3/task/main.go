package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"math/rand"
	"os"
	"time"

	gjs "github.com/brianvoe/gofakeit/v6"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке

}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(path string) error

	Append(item Commit)
	Insert(n int, c Commit) error
	Delete(n int) error
	DeleteCurrent() error
	Index() (int, error)
	Pop() *Node
	Shift() *Node
	SearchUUID(string) *Node
	Search(string) *Node
	Reverse() *DoubleLinkedList
	PrintAll()
}

var (
	ErrOutOfRange  = errors.New("out of range")
	ErrListIsEmpty = errors.New("empty double linked list")
)

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	dataByte, err := os.ReadFile(path)
	if err != nil {
		return err
	}

	commits, err1 := JsonUnmarshal(dataByte)
	if err1 != nil {
		return err1
	}

	commits = QuickSort(commits)

	for _, item := range commits {
		d.Append(item)
	}

	return nil
}

func (d *DoubleLinkedList) Append(item Commit) {
	node := &Node{data: &item, prev: nil, next: nil} //, prev: d.curr, next: d.tail}

	if d.len == 0 {
		d.head = node
		d.tail = node
		//node.prev = node

	} else {

		p := d.head
		for p.next != nil {
			p = p.next
		}
		node.prev = p
		d.curr = d.tail
		d.curr.next = node
		d.tail = node
	}

	d.len++
	return
}

func JsonUnmarshal(data []byte) (Commits, error) {
	var JsonText Commits
	err := json.Unmarshal(data, &JsonText)
	return JsonText, err
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	return d.len
}

// Current получение текущего элемента// ТУТ ВОПРОСЫ
func (d *DoubleLinkedList) Current() *Node {
	fmt.Println(d.curr.data)
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	if next := d.curr.next; d.curr != d.tail {
		curr := d.curr
		d.curr, d.curr.next = next, curr
		return next
	}
	fmt.Println("Текущее значение - последние")
	return nil
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	if prev := d.curr.prev; d.curr != d.head {
		d.curr, d.curr.prev = d.curr.prev, d.curr
		return prev
	}
	fmt.Println("Текущее значение - первое")
	return nil
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	node := &Node{data: &c}
	if n < 0 {
		return ErrOutOfRange
	}

	if d.len == 0 || n >= d.len {
		d.Append(c)
	} else {
		for i := 0; i < n; i++ {
			d.Next()
		}
		oldNode := node

		node.prev = oldNode.prev
		node.next = oldNode

		oldNode.prev.next = node
		oldNode.prev = node

		d.len++
	}
	return nil //Вот тут необходимо разобратсья
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {

	if d.Len() == 0 {
		return ErrListIsEmpty
	}
	if n < 0 || n >= d.len {
		return ErrOutOfRange
	}
	if n == 0 {
		d.Shift()
		return nil

	}
	if n == d.len-1 {
		d.Pop()
		return nil
	}
	curr := d.head
	for i := 0; i < n; i++ {
		curr = curr.next
	}
	_ = d.DeleteCurrent()
	d.len--
	return nil
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	if d.curr == nil {
		return ErrListIsEmpty
	}
	if d.curr != d.head || d.curr != d.tail {
		d.curr.prev.next = d.curr.next
		d.curr.next.prev = d.curr.prev
		d.len--
		return nil
	}
	if d.curr == d.head {
		d.Shift()

		return nil
	}
	if d.curr == d.tail {
		d.Pop()
		return nil
	}
	return nil
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	if d.len == 0 {
		return -1, ErrListIsEmpty
	}
	count := 0
	countNode := d.head
	if d.curr == d.head {
		return 0, nil
	}
	if d.curr == d.tail {
		return d.len - 1, nil
	}
	for countNode.next != d.curr {
		count++
		countNode = countNode.next
	}
	return count + 1, nil
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() *Node {
	if d.len == 0 {
		return nil
	}

	data := d.tail
	d.tail = d.tail.prev
	d.curr = d.tail
	if d.tail != nil {
		d.tail.next = nil
	} else {
		d.head = nil
	}
	d.len--
	return data
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() *Node {
	if d.len == 0 {
		return nil
	}
	data := d.head
	d.head = d.head.next
	d.curr = d.head
	if d.head != nil {
		d.head.prev = nil
	} else {
		d.tail = nil
	}
	d.len--
	return data
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	if d.len == 0 {
		fmt.Println("Пустой список")
		return nil
	}
	data := d.head

	for data != nil {
		if data.data.UUID == uuID {
			return data
		}
		data = data.next
	}
	return nil
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	if d.len == 0 {
		fmt.Println("Пустой список")
		return nil
	}
	data := d.head

	for data != nil {
		if data.data.Message == message {
			return data
		}
		data = data.next
	}
	return nil
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {

	currentNode := d.head
	nextNode := d.head
	var prev *Node
	for nextNode != nil {
		nextNode = nextNode.next
		currentNode.next = prev
		prev = currentNode
		currentNode = nextNode
	}
	// temp := l.head
	d.head, d.tail = d.tail, d.head
	// l.tail = temp
	p := prev
	//fmt.Println(d.head.data)
	for p != nil {
		//fmt.Print(p.data, " ")
		p = p.next
	}
	//fmt.Println("")
	//fmt.Println(d.tail.data)
	return d
}

func (d *DoubleLinkedList) PrintAll() {
	if d.len == 0 {
		return
	}
	i := 1
	curr := d.head
	for curr != nil {
		fmt.Printf("%d => %v\n", i, curr.data)
		i++
		curr = curr.next
	}
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

type Commits []Commit

func GenerateJSON() {
	// Дополнительное задание написать генератор данных
	// используя библиотеку gofakeit

	type generateJSON struct {
		Message string    `fake:"{sentence:5}" json:"message"`
		Uuid    string    `json:"uuid"`
		Date    time.Time `json:"date"`
	}

	dateJSON := make([]generateJSON, 100)
	for i, product := range dateJSON {
		_ = gjs.Struct(&product)
		product.Uuid = gjs.UUID()
		dateJSON[i] = product
	}
	jsonByte, _ := json.Marshal(dateJSON)
	_ = os.WriteFile("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module3/task/testGJS.json",
		jsonByte, 0666)

}

func QuickSort(path Commits) Commits {
	if len(path) < 1 {
		return path
	}

	median := path[rand.Intn(len(path))].Date.Year()

	lowPart := make(Commits, 0, len(path))
	mediumPart := make(Commits, 0, len(path))
	highPart := make(Commits, 0, len(path))

	for _, item := range path {
		switch {
		case item.Date.Year() < median:
			lowPart = append(lowPart, item)
		case item.Date.Year() == median:
			mediumPart = append(mediumPart, item)
		case item.Date.Year() > median:
			highPart = append(highPart, item)
		}
	}

	lowPart = QuickSort(lowPart)
	highPart = QuickSort(highPart)

	lowPart = append(lowPart, mediumPart...)
	lowPart = append(lowPart, highPart...)
	return lowPart

}

func main() {
	GenerateJSON()

	var user LinkedLister = &DoubleLinkedList{}
	_ = user.LoadData("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module3/task/testGJS.json")
	user.PrintAll()

	//d := &DoubleLinkedList{}
	//LinkedLister()
	//
	//_ = d.LoadData("/Users/admin/go/src/gitlab.com/a3buka/go-kata/module3/task/testGJS.json")

}
